import { Component } from '@angular/core';
import { Vibration } from '@ionic-native/vibration/ngx';
import {NavController} from '@ionic/angular';
import anime from 'animejs/lib/anime.es';


@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

    plusBtnAnimation = anime({
        targets: '#addTaskBTN',
        rotate: '2turn',
        easing: 'easeInOutSine'
    });

  markerDisplay = 0;
  constructor(public navCtrl: NavController, private vibration: Vibration) {
      setTimeout(() => { this.buildMenu(); }, 2000);

  }
  addTaskItem() {
      this.navCtrl.navigateForward('tabs/tab2');
      this.mainMenu();
      this.locSelMode();
  }
    profileItem() {
        this.navCtrl.navigateForward('my-profile');
        this.mainMenu();
        this.locSelMode();
    }

  locSelMode() { // enter on choose-location mode
    const menuBtn = document.getElementById('menuBTN');
    const arrowBtn = document.getElementById('addTaskBTN');
    const backBTN = document.getElementById('backBTN');
    menuBtn.hidden = !menuBtn.hidden;
    arrowBtn.hidden = !arrowBtn.hidden;
    backBTN.hidden = !backBTN.hidden;
    setTimeout(() => {
      const styleElem = document.head.appendChild(document.createElement('style'));
         if ( this.markerDisplay === 1) {
              styleElem.innerHTML = '.map:after {  opacity: 0 !important;}';
              this.markerDisplay = 0;
         } else {
             styleElem.innerHTML = '.map:after { opacity: 100 !important;}';
             this.markerDisplay = 1;
         }
     }, 300);
  }

  mainMenu( clickOnMap = false) { // close and open menu bar
      const btn = document.getElementById('menuBTN');
      if (!btn.classList.contains('open') && clickOnMap === false) {
          this.vibration.vibrate(10);
          anime({
              targets: '#menuBTN',
              fontSize: '22pt',
              duration: 200,
              easing: 'spring(1, 80, 10, 0)'
          });
          anime({
              targets: '#backMenu',
              bottom: '0',
              duration: 200,
              easing: 'easeInOutCirc'
          });
          anime({
              targets: '.Menu',
              bottom: '-65px',
              duration: 200,
              easing: 'cubicBezier(.5, .05, .1, .3)'
          });
          btn.classList.remove('closed');
          btn.classList.add('open');
      } else {
          anime({
              targets: '#menuBTN',
              fontSize: '10pt',
              duration: 200,
              easing: 'spring(1, 80, 10, 0)'
          });
          anime({
              targets: '#backMenu',
              bottom: '-150px',
              duration: 200,
              easing: 'easeInOutCirc'
          });
          anime({
              targets: '.Menu',
              bottom: '-200px',
              duration: 200,
              easing: 'cubicBezier(.5, .05, .1, .3)'
          });
          btn.classList.remove('open');
          btn.classList.add('closed');

      }
  }

  buildMenu() { // make cirle menu items
      const divs = document.getElementsByTagName('ion-tab-button');
      const delta = Math.PI * 2 / (divs.length * 1.62);
      const x = 0, y = 0;
      let angle = 0;

      for (let i = 0; i < divs.length; i++) {
          divs[i].style.position = 'absolute';
          divs[i].style.zIndex = '600';
          divs[i].style.left =  120 * Math.cos(angle) + 'px';
          divs[i].style.top = 75 * Math.sin(angle) + 'px';

          angle -= delta;
      }
  }
    next() {
      this.navCtrl.navigateForward('/add-task');
      this.locSelMode();
    }
}


