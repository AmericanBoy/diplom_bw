import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TabsPageRoutingModule } from './tabs.router.module';
import { Vibration } from '@ionic-native/vibration/ngx';


import { TabsPage } from './tabs.page';

// import { FontAwesomeModule } from '@fortawesome/fontawesome-free';
 // import { faCoffee } from '@fortawesome/free-solid-svg-icons';



@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TabsPageRoutingModule,
  ],
  providers: [
      Vibration
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {
  // faCoffee = faCoffee;
}
