import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'tab1',
        children: [
          {
            path: '',
            loadChildren: '../tab1/tab1.module#Tab1PageModule'
          }
        ]
      },
      {
        path: 'tab2',
        children: [
          {
            path: '',
            loadChildren: '../tab2/tab2.module#Tab2PageModule'
          }
        ]
      },
      {
        path: 'tab3',
        children: [
          {
            path: '',
            loadChildren: '../tab3/tab3.module#Tab3PageModule'
          }
        ]
      },
      {
        path: 'name-login',
        children: [
          {
            path: '',
            loadChildren: '../name-login/name-login.module#NameLoginPageModule'
          }
        ]
      },
      {
        path: 'taken-task',
        children: [
          {
            path: '',
            loadChildren: '../taken-task/taken-task.module#TakenTaskPageModule'
          }
        ]
      },
      {
        path: 'shop-page',
        children: [
          {
            path: '',
            loadChildren: '../shop-page/shop-page.module#ShopPagePageModule'
          }
        ]
      },
     /* {
        path: 'add-task',
        children: [
          {
            path: '',
            loadChildren: '../add-task/add-task.module#AddTaskPageModule'
          }
        ]
      },*/
      {
        path: 'alert-page',
        children: [
          {
            path: '',
            loadChildren: '../alert-page/alert-page.module#AlertPagePageModule'
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/tab2',
        pathMatch: 'full'
      },
      {
        path: 'proposes',
        redirectTo: '/proposes',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tutoral',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
