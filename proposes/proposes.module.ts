import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ProposesPage } from './proposes.page';
import {ProposesModalPage} from '../proposes-modal/proposes-modal.page';

const routes: Routes = [
  {
    path: '',
    component: ProposesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ProposesPage, ProposesModalPage],
  entryComponents: [ProposesModalPage],

})
export class ProposesPageModule {}
