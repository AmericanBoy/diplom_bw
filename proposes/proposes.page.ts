import { Component, OnInit } from '@angular/core';
import {ProposesModalPage} from '../proposes-modal/proposes-modal.page';
import {ModalController, NavController} from '@ionic/angular';

import anime from 'animejs/lib/anime.es';
import {AboutProposeModalPage} from '../about-propose-modal/about-propose-modal.page';

@Component({
  selector: 'app-proposes',
  templateUrl: './proposes.page.html',
  styleUrls: ['./proposes.page.scss'],
})
export class ProposesPage implements OnInit {

  constructor(public modalController: ModalController,  public navCtrl: NavController) { }

  ngOnInit() {
  }
  ionViewWillEnter() {
    anime({
      targets: '.animHeader',
      translateX: -10,
      opacity: 1,
      delay: anime.stagger(100, {start: 500})
    });
    anime({
      targets: 'ion-card',
      translateY: -50,
      opacity: 1,
      delay: anime.stagger(100, {start: 500})
    });
  }

  async onModal() {
    const modal = await this.modalController.create({
      component: ProposesModalPage,
      componentProps: {
      },
      cssClass: 'proposes-modal'
    });
    return await modal.present();
  }
}
