import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProposesPage } from './proposes.page';

describe('ProposesPage', () => {
  let component: ProposesPage;
  let fixture: ComponentFixture<ProposesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProposesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProposesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
