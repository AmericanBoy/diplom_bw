import { Component, OnInit } from '@angular/core';
import {NavController} from '@ionic/angular';
import {Storage} from '@ionic/storage';
import {HTTP} from '@ionic-native/http/ngx';


@Component({
  selector: 'app-alert-page',
  templateUrl: './alert-page.page.html',
  styleUrls: ['./alert-page.page.scss'],
})
export class AlertPagePage  {
  dataInicial: Date;

  constructor(public navCtrl: NavController,  private storage: Storage, private http: HTTP) {
    this.checkedDate();
    this.auth();
    setTimeout(() => {
      document.getElementById('tabs').style.display = 'none';
      document.getElementById('goToLocSelBTN').style.display = 'none';
    }, 30);
  }

  next() {
    this.navCtrl.navigateForward('tabs/name-login');
  }

  auth() {
    this.storage.get('code').then(
        (result) => {
          this.sendCode(result);
        }
    );
  }

  checkedDate() {
    this.dataInicial = new Date();
    const day = this.dataInicial.getDate();
    const month = this.dataInicial.getMonth() + 1;
    const year = this.dataInicial.getFullYear();
    if (day > 10 || month !== 6 || year !== 2019) {
      setTimeout(() => {
        document.getElementById('nextBtn').hidden = true;
      }, 1000);
      this.navCtrl.navigateForward('about');
    }
  }
  sendCode(code) {
    this.http.post('https://beeworker.com.ua/php/checkCode.php', {
      'code': code,
    }, {}).then(result => {
      this.redirect(result);
    }).catch(error => {
      console.log(error);
    });
  }
  redirect(result) {
    if (result.data === 'true') {
      this.navCtrl.navigateForward('tabs/tab2');
    }
  }


}
