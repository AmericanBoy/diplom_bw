import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LookAddressTaskPage } from './look-address-task.page';

describe('LookAddressTaskPage', () => {
  let component: LookAddressTaskPage;
  let fixture: ComponentFixture<LookAddressTaskPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LookAddressTaskPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LookAddressTaskPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
