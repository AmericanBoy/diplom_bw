import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { LookAddressTaskPage } from './look-address-task.page';



const routes: Routes = [
  {
    path: '',
    component: LookAddressTaskPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [LookAddressTaskPage]
})
export class LookAddressTaskPageModule {}
