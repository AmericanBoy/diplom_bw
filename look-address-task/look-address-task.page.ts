import { Component, OnInit } from '@angular/core';
import { NavParams } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import anime from 'animejs/lib/anime.es';
import {mainMap} from 'src/app/MapGenerationClass/mainMap';
import leaflet from 'leaflet';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  Marker,
  ILatLng,
  MarkerCluster,
  MarkerOptions,
  GoogleMapsAnimation,
  MyLocation,
  MyLocationOptions,
  LocationService,
  Environment,
  CameraPosition,
  HtmlInfoWindow
} from '@ionic-native/google-maps';
import {mapStyle} from 'src/app/tab2/mapStyle.js';
import { Storage } from '@ionic/storage';

declare var google: any;

@Component({
  selector: 'app-look-address-task',
  templateUrl: './look-address-task.page.html',
  styleUrls: ['./look-address-task.page.scss'],
})

export class LookAddressTaskPage {
  map: GoogleMap; // карта
  items: []; // стили карты
  late: any;
  long: any;
  newMarkerLocation: any;
  myLocationMarker_lat: any;
  myLocationMarker_lng: any;
  geocoder: any;
  map2: any;
  latTask: any;
  lngTask: any;

  constructor(public storage: Storage, private mainmap: mainMap, public modal: ModalController, public navParams: NavParams) {
    setTimeout(()=> {
      let test = document.getElementById('map_canvas_look_location');
      console.log(test);
      this.storage.get('locationTask').then(
        (result) => {
            this.latTask = result.lat;
            this.lngTask = result.lng;
            this.loadLeafMap(parseFloat(this.latTask), parseFloat(this.lngTask), test);
            console.log('resAdress = ' + result);
          }
        );
    }, 300);
    //this.mainmap.loadMap(50.23, 28.29, "map_canvas_change");
  }

  loadMap(latTask, lngTask, nameDivId) {
    alert(nameDivId);
    console.log("peredal = " + latTask + " " + lngTask);
    Environment.setEnv({
        'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyBqQbmzhu1f9KvhMSO9DkUDIO8bDaCJ9_I',
        'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyBqQbmzhu1f9KvhMSO9DkUDIO8bDaCJ9_I'
    });

    let coord = {lat: latTask, lng: lngTask};
    /* Настройки карты */
    this.items = mapStyle;
    this.map = GoogleMaps.create(nameDivId, {
      camera: {
        target: coord,
        zoom: 14,
        tilt: 45
      },
      styles: this.items,
      disableDefaultUI: true,
      gestureHandling: 'none',
      gestures: {
          scroll: false,
          tilt: false,
          rotate: false,
          zoom: false
        },
      controls: {
        compass: false,
        myLocationButton: false,
        indoorPicker: false,
        zoom: false // Only for Android
      }
    });

    this.map.on( GoogleMapsEvent.MAP_DRAG_END ).subscribe(() => {
      this.newMarkerLocation = this.map.getCameraPosition().target;
      console.log("center = " + this.newMarkerLocation);
    });

    let marker: Marker = this.map.addMarkerSync({
      icon:{
        url: './assets/icon/point.png',
        size: {
          width: 60,
          height: 70
        }
      },
      position: coord,
      animation: GoogleMapsAnimation.BOUNCE,
      disableAutoPan: true
    });
  }

  loadLeafMap(lat, lng, idDivMap){
    alert(lat + " " + lng + " " + idDivMap);
    this.map2 = leaflet.map(idDivMap, {zoomControl:false, attributionControl: false}).setView([lat, lng], 12);
    leaflet.tileLayer('https://api.mapbox.com/styles/v1/rikind/cjzvakb3v0z081epye72gq5m0/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoicmlraW5kIiwiYSI6ImNqenZhYWJsNjAya2MzY21ycHp6amFocmoifQ.6Fm4Du5SOUv1AUz6NC4F8Q', {
      attributions: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
      zoom: 13,
      setView: true
    }).addTo(this.map2);

    var greenIcon = leaflet.icon({
      iconUrl: '../../assets/icon/point.png',
      iconSize: [60, 70]
    });

    var marker = leaflet.marker([lat, lng], {icon: greenIcon}).addTo(this.map2);
    var marker2 = leaflet.marker([lat, lng], {icon: greenIcon}).addTo(this.map2);
    this.map2.locate({
      watch: true
    }).on('locationfound', (e) => {
      if (!marker) {
        marker = leaflet.marker([e.latitude, e.longitude], { icon: greenIcon }).addTo(this.map2);
      } else {
        this.myLocationMarker_lat = e.latitude;
        this.myLocationMarker_lng = e.longitude;
        marker.setLatLng([e.latitude, e.longitude]);
      }
  });
    //this.map2.dragging.disable();
  }

  mylocation(){
    alert(this.myLocationMarker_lat + " " + this.myLocationMarker_lng);
    this.map2.setView([this.myLocationMarker_lat, this.myLocationMarker_lng], 20);
  }

  myLocation(){
    this.map.getMyLocation().then((location) => {
      let position: CameraPosition<ILatLng> = {
        target: location.latLng,
        zoom: 16
      };
      
      // move the map's camera to position
      this.map.animateCamera(position).then(() => {
      });
    });
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modal.dismiss({
      'dismissed': true
    });
  }
 
}
