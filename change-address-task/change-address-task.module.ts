import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ChangeAddressTaskPage } from './change-address-task.page';



const routes: Routes = [
  {
    path: '',
    component: ChangeAddressTaskPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ChangeAddressTaskPage]
})
export class ChangeAddressTaskPageModule {}
