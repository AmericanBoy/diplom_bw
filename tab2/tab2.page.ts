import {Component, OnInit} from '@angular/core';
import {ElementRef, NgZone, ViewChild} from '@angular/core';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import 'rxjs/add/operator/map';
import { LoadingController } from '@ionic/angular';
import 'rxjs/add/operator/toPromise';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {mapStyle} from 'src/app/tab2/mapStyle.js';
import {HTTP} from '@ionic-native/http/ngx';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
import {TabsPage} from '../tabs/tabs.page';
import { NavigationExtras } from '@angular/router';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import {
    ToastController,
    Platform
} from '@ionic/angular';
import {
    GoogleMaps,
    GoogleMap,
    GoogleMapsEvent,
    Marker,
    MarkerCluster,
    MarkerOptions,
    GoogleMapsAnimation,
    MyLocation,
    MyLocationOptions,
    LocationService,
    Environment,
    GoogleMapOptions,
    HtmlInfoWindow,
    GoogleMapsMapTypeId
} from '@ionic-native/google-maps';
import { forEach } from '@angular/router/src/utils/collection';
import anime from 'animejs/lib/anime.es';
import * as Hammer from 'hammerjs';
import {CustomHammerConfig} from '../app.module';
declare var google: any;


@Component({
    selector: 'app-tab2',
    templateUrl: 'tab2.page.html',
    styleUrls: ['tab2.page.scss'],
    providers: [DatePipe]
})

export class Tab2Page {

    @ViewChild('Map') mapElement: ElementRef;
    @ViewChild('slides') slides;

    /*Переменные*/
    items: []; // стили карты
    map: GoogleMap; // карта
    mapOptions: any; // настройки карты
    location = {lat: null, lng: null}; // хранит долготу и широту
    location_my_track = {lat: null, lng: null};
    markerOptions: any = {position: null, map: null, title: null}; // Эта переменная хранит настройки маркера и все что с ним связано
    marker: any; // маркер
    apiKey: any = 'AIzaSyBqQbmzhu1f9KvhMSO9DkUDIO8bDaCJ9_I'; /*API Key*/
    newMarkerLocation: any; // хранит в каких координатах спавнить новыйы маркер
    geocoder: any;
    streetInfo: any;


    // Штуки для показа в прототипе - потом удаляться
    myLocationMarkerID: any;

    myLocationMarker = {lat: 50.245166, lng: 28.664462};

    markers = [];
    DEMO_filter_markers = [];
    DEMO_new_markers = [];
    date: any;
    money: any;
    time: any;
    type: any;
    description: any = '';
    latAbouTask: any;
    lngAbouTask: any;
    DEMO_last_id: any;
    late: any;
    long: any;
    navigationextr;
    ID: any;

    shrtTask = [];

    htmlInfoWindow = new HtmlInfoWindow();

    buttonColor: string ="white";
    rangeOldPosition = 100;

    slideOpts = {
        initialSlide: 0,
        speed: 500,
        zoom: {
            toggle: false
        }
    };

    constructor(public zone: NgZone, public geolocation: Geolocation, public http: HTTP,
                private nativeGeocoder: NativeGeocoder, public tabs: TabsPage,
                public loadingController: LoadingController, public storage: Storage, private router: Router,
                public loadingCtrl: LoadingController,
                public toastCtrl: ToastController,
                private platform: Platform,
                private datePipe: DatePipe) {
        this.storage.set('id_user', 11);
        setTimeout(() => {
            this.slides.lockSwipes(true);
        }, 1000);
    }

    // Метод которая сраб. после инициализации представления компонентов, в которой вызываеться метод создания карты
    // tslint:disable-next-line:use-life-cycle-interface
    async ngOnInit() {

        await this.platform.ready().then(() => {

            const mc = new Hammer(document.getElementById('animBtn'));
            mc.add( new Hammer.Pan({ direction: Hammer.DIRECTION_ALL, threshold: 0 }) );
            mc.on('pan', handleDrag);
            let lastPosX = 0;
            let lastPosY = 0;
            let isDragging = false;
            function handleDrag(ev) {
                const elem = ev.target;

                if ( ! isDragging ) {
                    isDragging = true;
                    lastPosX = elem.offsetLeft;
                    lastPosY = elem.offsetTop;
                }

                const posY = ev.deltaY + lastPosY;

                if (posY <= 230 && posY >= 70 ) {
                    elem.style.top = posY + 'px';
                }
                menuIconAnim.seek(menuIconAnim.duration - ((Number(elem.style.top.slice(0, -2)) - 70) / 140) * 1000);
                buttonsAnim.seek(buttonsAnim.duration - ((Number(elem.style.top.slice(0, -2)) - 70) / 140) * 1000);
                backAnim.seek(backAnim.duration - ((Number(elem.style.top.slice(0, -2)) - 70) / 140) * 1000);
                avatarAnim.seek(avatarAnim.duration - ((Number(elem.style.top.slice(0, -2)) - 70) / 140) * 1000);
                pacInputAnim.seek(pacInputAnim.duration - ((Number(elem.style.top.slice(0, -2)) - 70) / 140) * 1000);
                headerAnim.seek(headerAnim.duration - ((Number(elem.style.top.slice(0, -2)) - 70) / 140) * 1000);
                cntrlBtnAnim.seek(cntrlBtnAnim.duration - ((Number(elem.style.top.slice(0, -2)) - 70) / 140) * 1000);
                if (ev.isFinal) {
                    isDragging = false;
                }
                console.log(((Number(elem.style.top.slice(0, -2)) - 70) / 140));
                if (isDragging === false && ((Number(elem.style.top.slice(0, -2)) - 70) / 140) >= 0.5) {
                    let j = menuIconAnim.duration - ((Number(elem.style.top.slice(0, -2)) - 70) / 140) * 1000;
                    setInterval(() => {
                        if (j > 0) {
                            menuIconAnim.seek(j);
                            buttonsAnim.seek(j);
                            backAnim.seek(j);
                            avatarAnim.seek(j);
                            pacInputAnim.seek(j);
                            headerAnim.seek(j);
                            cntrlBtnAnim.seek(j);
                            j -= 50;
                        }
                    }, 20);
                    elem.style.top = 230 + 'px';
                }
                if (isDragging === false && ((Number(elem.style.top.slice(0, -2)) - 70) / 140) < 0.5) {
                    let i = menuIconAnim.duration - ((Number(elem.style.top.slice(0, -2)) - 70) / 140) * 1000;
                    setInterval(() => {
                        if (i < 1000) {
                            menuIconAnim.seek(i);
                            buttonsAnim.seek(i);
                            backAnim.seek(i);
                            avatarAnim.seek(i);
                            pacInputAnim.seek(i);
                            headerAnim.seek(i);
                            cntrlBtnAnim.seek(i);

                            i += 50;
                        }
                    }, 20);
                    elem.style.top = 70 + 'px';
                }
            }


            const _this = this;

            const menuIconAnim = anime({
                targets: '.workNearYou',
                translateY: -200,
                duration: 1000,
                easing: 'cubicBezier(.5, .05, .1, .3)'
            });
            const buttonsAnim = anime({
                targets: 'ion-buttons',
                translateY: -500,
                opacity: 0,
                duration: 1000,
                easing: 'cubicBezier(.5, .05, .1, .3)'
            });
            const backAnim =  anime({
                targets: '.searchHeader',
                height: 100,
                duration: 1000,
                easing: 'cubicBezier(.5, .05, .1, .3)'
            });
            const headerAnim = anime({
                targets: '.searchHeader .header',
                opacity: 0,
                duration: 1000,
                easing: 'cubicBezier(.5, .05, .1, .3)'
            });
            const pacInputAnim = anime({
                targets: '#pac-input',
                width: '65vw',
                translateY: -85,
                translateX: 40,
                duration: 1000,
                easing: 'cubicBezier(.5, .05, .1, .3)'
            });
            const avatarAnim = anime({
                targets: 'ion-avatar',
                width: 80,
                height: 80,
                paddingTop: 20,
                paddingRight: 20,
                paddingBottom: 20,
                paddingLeft: 22,
                translateY: -25,
                translateX: -140,
                duration: 1000,
                easing: 'cubicBezier(.5, .05, .1, .3)'
            });
            const cntrlBtnAnim = anime({
                targets: '#animBtn',
                rotate: 180,
                duration: 500,
                easing: 'cubicBezier(.5, .05, .1, .3)'
            });



            const slider = this.slides;
            this.presentLoading();
            const seekProgressEl = document.querySelector('.progress') as HTMLInputElement;
            const prevCurPos = 0;
        });

        await this.loadMap();
    }
    curCursorPos() {
        const seekProgressEl = document.querySelector('.progress') as HTMLInputElement;
        this.rangeOldPosition = Number(seekProgressEl.value);
    }
    // Этот метод генерирует собствено саму карту
    loadMap() {
        this.storage.set('newAddTask', 0);
        Environment.setEnv({
            'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyBqQbmzhu1f9KvhMSO9DkUDIO8bDaCJ9_I',
            'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyBqQbmzhu1f9KvhMSO9DkUDIO8bDaCJ9_I'
        });
        /*загружаем гугл карту*/
        const script = document.createElement('script');
        script.id = 'googleMap';
        if (this.apiKey) {
            script.src = 'https://maps.googleapis.com/maps/api/js?key=' + this.apiKey + '&libraries=places';
        } else {
            script.src = 'https://maps.googleapis.com/maps/api/js?key=';
        }
        document.head.appendChild(script);

        /* Узнаем позицию пользователя */
        this.geolocation.getCurrentPosition().then((position) => {
            this.location.lat =  position.coords.latitude;
            this.location.lng = position.coords.longitude;
        });
        /* Настройки карты  */
        this.items = mapStyle;
            this.map = GoogleMaps.create('map_canvas', {
                //mapType: GoogleMapsMapTypeId.HYBRID,
                camera: {
                    target: {
                        lat: 50.2577,
                        lng: 28.6382
                    },
                    zoom: 18,
                    tilt: 30
                },
                styles: this.items,
                controls: {
                    myLocation: true
                }
            });
            const _this = this;
            //_this.searchBox(_this.map);

            this.map.one( GoogleMapsEvent.MAP_READY ).then( ( data: any ) => {
                this.storage.remove('MyTasks');
                this.searchBox(this.map);
                this.demoMyPos();
                setTimeout(() => {this.addCluster(this.markers, this.map, this)}, 500);
                console.log("my Markers = " + this.markers);
                let mycoords: any;
                setInterval(()=>{
                    this.trackAllTimeNewUserTask();
                }, 500);
                /*setInterval(function() {
                    let options: MyLocationOptions = {
                        enableHighAccuracy: true
                      };

                      LocationService.getMyLocation(options).then((location: MyLocation) => {
                        mycoords = location.latLng;
                        marker.setPosition(location.latLng);
                        console.log("my loc = " + location.latLng);
                      });
                }, 500);*/
            });

        this.map.on( GoogleMapsEvent.MAP_DRAG_END ).subscribe(() => {
            this.newMarkerLocation = this.map.getCameraPosition().target;
            console.log("center = " + this.newMarkerLocation);
            this.geocoder = new google.maps.Geocoder;
            this.geocodeLatLng(this.geocoder, this);
        });

        this.map.on(GoogleMapsEvent.MAP_CLICK).subscribe((params) => {
            console.log("closeee + " + this.htmlInfoWindow);
            this.htmlInfoWindow.close();
        });
    }

    goToPurse() {
        this.slides.lockSwipes(false);
        this.slides.slideTo(1);
        this.slides.lockSwipes(true);
        console.log(1);
        document.getElementById('headController').style.display = 'none';
    }

    goToMainSlide() {
        this.slides.lockSwipes(false);
        this.slides.slideTo(0);
        this.slides.lockSwipes(true);
        console.log(1);
        document.getElementById('headController').style.display = 'block';

    }

    geocodeLatLng(geocoder, __this) {
        const input = document.getElementById('pac-input') as HTMLInputElement;
        this.late = parseFloat(__this.newMarkerLocation.lat);
        this.long = parseFloat(__this.newMarkerLocation.lng);
        this.geocoder.geocode({location: __this.newMarkerLocation}, function (results, status) {
            if (status === 'OK') {
                if (results[0]) {
                    let loc = {lat: __this.late, lng: __this.long};
                    __this.storage.set('location', loc);
                    __this.storage.set('streetNameTask', results[0].formatted_address);
                    console.log('улица = ' + results[0].formatted_address);
                    console.log('local = ' + __this.late + ' ' + __this.long);
                    this.streetInfo = String(results[0].formatted_address);
                    // document.getElementById('pac-input').setAttribute = results[0].formatted_address;
                    input.value = results[0].formatted_address;
                    //console.log("input = " + document.getElementById('pac-input')[0]);
                    //console.log("input = " + document.getElementById('pac-input')[1]);
                } else {
                    console.log('No results found');
                }
            } else {
                console.log('Geocoder failed due to: ' + status);
            }

        });
    }

    searchBox(map) {
        const input = document.getElementById('pac-input');
        const input2 = document.getElementById('pac-input2') as HTMLInputElement;
        console.log("input = " + input);
        console.log("input2 = " + input2);
        const autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);

        autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);


        autocomplete.addListener('place_changed', function () {
            const place = autocomplete.getPlace();
            if (!place.geometry) {
                // Если такой улицы нету
                window.alert(' No details available for input: \'' + place.name + '\'');
                return;
            }
            console.log('locccc = ' + parseFloat(place.geometry.location.lat()));
            const locationn = {lat: null, lng: null};
            locationn.lat = place.geometry.location.lat();
            locationn.lng = place.geometry.location.lng();

            // Если улица есть то камеру ведем к ней
            if (place.geometry.viewport) {
                map.moveCamera({
                    target: locationn,
                    zoom: 17,
                    tilt: 30
                });
            } else {
                map.moveCamera({
                    target: locationn,
                    zoom: 17,
                    tilt: 30
                });
            }

            let address = '';
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].full_name || ''),
                    (place.address_components[1] && place.address_components[1].full_name || ''),
                    (place.address_components[2] && place.address_components[2].full_name || '')
                ].join(' ');
            }
        });
    }

    btnBack() {
        this.tabs.locSelMode();
    }

    roadToFullInfo() {
        console.log('IDDDD = ' + this.ID);
        const navigationExtras: NavigationExtras = {
            queryParams: {
                id: this.ID
            }
        };
        this.router.navigate(['about'], navigationExtras);
        // записать все в стораге
    }

    trackAllTimeNewUserTask(){
        this.storage.get('newAddTask').then( (result) => { if(result == 1){this.DEMOaddNewUserTask()}});
    }

    DEMOaddNewUserTask(){
        this.storage.set('newAddTask', 0);

        let data;
        let money;
        let type;
        let time;
        let descript;   
        let coords;

        this.storage.get('newDataTask').then( (result) => { data = result; });
        this.storage.get('newMoneyTask').then( (result) => { money = result; });
        this.storage.get('newTypeTask').then( (result) => { type = result; });
        this.storage.get('newTimeTask').then( (result) => { time = result; });
        this.storage.get('newDescriptionTask').then( (result) => { descript = result; });
        this.storage.get('newCoordsTask').then( (result) => { 
            coords = result;
            this.creatFakeMarkers(this.DEMO_last_id++, coords.lat, coords.lng, data, money, time, type, 'yborka', descript, 1);
            setTimeout(() => {this.addCluster(this.markers, this.map, this)}, 500); 
        });
    }

    trackMyPosition(_this) {
       // setInterval(_this.getMyPosotion, 1000);
        setInterval(function() {
            _this.geolocation.getCurrentPosition().then((position) => {
                _this.location_my_track.lat = position.coords.latitude;
                _this.location_my_track.lng = position.coords.longitude;
                console.log(_this.location_my_track.lat + ' ' + _this.location_my_track.lng);
        });
          }, 100);
    }

    //отвечает за фильтрацию задний на карте
    filterTasksOnMap(event, ctgrName){
       let tempArr = [];
        if(event.srcElement.style.background  != "red") {
            event.srcElement.style.background = 'red'
            for(let i = 0; i < this.markers.length; i++){
                console.log("mark = " + this.markers[i].typeT);
                if(this.markers[i].typeT == ctgrName){
                    this.DEMO_filter_markers.push(this.markers[i]); 
                    tempArr.push(i);
                    //bag похоуду
                    //this.markers.splice(i,1);
                }
            }
            console.log("DEMO_filter_markers1 = " + this.DEMO_filter_markers.length);
            this.map.clear();
            if(this.DEMO_filter_markers.length != 0){
                setTimeout(() => {this.addCluster(this.DEMO_filter_markers, this.map, this)}, 500);
            }
            else{
                setTimeout(() => {this.addCluster(this.markers, this.map, this)}, 500);
            }
        } else {
            event.srcElement.style.background = 'white';
            let first_i;
            let last_i;
            for(let i = 0; i < this.DEMO_filter_markers.length; i++){
                console.log("mark = " + this.markers[i].typeT);
                if(this.DEMO_filter_markers[i].typeT == ctgrName){
                    if(!(first_i >= 0)){
                        first_i = i;
                    }
                    last_i = i;
                }
            }
            console.log("first_i = " + first_i + " " + last_i);
            console.log("DEMO_filter_markers2 = " + this.DEMO_filter_markers.length);
            this.DEMO_filter_markers.splice(first_i, (last_i-first_i)+1);
            console.log("DEMO_filter_markers3 = " + this.DEMO_filter_markers.length);
            this.map.clear();
            if(this.DEMO_filter_markers.length != 0){
                setTimeout(() => {this.addCluster(this.DEMO_filter_markers, this.map, this)}, 500);
            }
            else{
                setTimeout(() => {this.addCluster(this.markers, this.map, this)}, 500);
            }
        }
          
    }

    //выводит маркеры на карту вместе с кластерезацией
    addCluster(data, map, _this) {
        this.map.clear();
        const markerCluster: MarkerCluster = _this.map.addMarkerClusterSync({
          boundsDraw: false,
          markers: data,
          icons: [
            {
              min: 2,
              url: './assets/icon/pin-group_main.png',
              label: {
                color: "white"
              }
            }
          ]
        });
        
        markerCluster.on(GoogleMapsEvent.MARKER_CLICK).subscribe((params) => {
            this.htmlInfoWindow.close();
            let marker: Marker = params[1];
            this.storage.set('id_task', marker.get('id'));
            this.ID = marker.get('id');
            //alert("info = " + marker.get('type') + " " + marker.get('id'));
            this.date = marker.get('date');
            this.money = marker.get('money');
            this.time = marker.get('time');
            this.type = marker.get('typeT');
            this.description = marker.get('descript');
            let coord = marker.get('position');
            //alert("asddadasd = " + coord.lat)
            this.storage.set('dateDemoTask', this.date);
            this.storage.set('moneyDemoTask', this.money);
            this.storage.set('timeDemoTask', this.time);
            this.storage.set('typeDemoTask', this.type);
            this.storage.set('descriptDemoTask', this.description);
            this.storage.set('coordsDemoTask', coord);
            this.infoWindowSetDataTask(this.htmlInfoWindow, marker);
           
            console.log("test33 = " + marker.get('id') + " " + params[0].lat);
            let loc = {lat: params[0].lat, lng: params[0].lng};
            this.storage.set('locationTask', loc);
        });
        
        //markerCluster.trigger(GoogleMapsEvent.MARKER_CLICK);
    }

    //при клике на меркер выводит инфовиндов(краткую информация о заданиии)
    infoWindowSetDataTask(htmlInfoWindow, marker){
        const frame: HTMLElement = document.createElement('div');
        setTimeout(() => {
            const inf = document.getElementById('infoWindowBack');
            console.log("inf = " + inf);
            frame.innerHTML = inf.innerHTML;
            frame.getElementsByTagName('ion-button')[0].addEventListener('click', () => {this.roadToFullInfo(); });
            htmlInfoWindow.setBackgroundColor('#fdcf45');
            htmlInfoWindow.setContent(frame, {
                width: '200px',
                height: '210px',
            });
            
            const backInfoWindow = document.head.appendChild(document.createElement('style'));
            backInfoWindow.innerHTML = '.pgm-html-info-content-frame {  border: none !important;' +
                'border-radius: 20px;}' +
                'pgm-html-info-content-box{ width: 95% !important;}' +
                '.liFontawesome{\n' +
                '  font-family:  "Font Awesome", sans-serif;\n' +
                '  color: black;\n' +
                '  font-size: 13pt;\n' +
                '}' +
                '.liText{ ' +
                '  font-family:  Montserrat-RL, sans-serif;\n' +
                '  color: black;\n' +
                '  font-size: 13pt;' +
                '}' +
                '.shTaskBtn{\n' +
                '  font-family:  Montserrat-L, sans-serif;\n' +
                '  color: #fdd732;\n' +
                '  font-size: 13pt;\n' +
                '}' +
                '.liTextBold{\n' +
                '  font-family:  Montserrat-R, sans-serif;\n' +
                '  color: black;\n' +
                '  font-size: 13pt;\n' +
                '}';
            htmlInfoWindow.open(marker);
        }, 1000);
    }

    demoMyPos(){
        LocationService.getMyLocation().then((myLocation: MyLocation) => {
            let myCoords = myLocation.latLng;
            //alert("mypos = " + myCoords);
            for(let i = 0; i < 30; i++){
                let dayVrem = Math.floor(Math.random() * (Math.floor(28) - Math.ceil(1) + 1)) + Math.ceil(1);
                let day = dayVrem < 10 ? "0" + dayVrem : dayVrem;
                let monthVrem = Math.floor(Math.random() * (Math.floor(12) - Math.ceil(1) + 1)) + Math.ceil(1);
                let month = monthVrem < 10 ? "0" + monthVrem : monthVrem;
                let time_hVrem = (Math.floor(Math.random() * (Math.floor(24) - Math.ceil(1) + 1)) + Math.ceil(1));
                let time_h = time_hVrem < 10 ? "0" + time_hVrem : time_hVrem;
                let time_mVrem = Math.floor(Math.random() * (Math.floor(59) - Math.ceil(1) + 1)) + Math.ceil(1);
                let time_m = time_mVrem < 10 ? "0" + time_mVrem : time_mVrem;
                let type = Math.floor(Math.random() * (Math.floor(4) - Math.ceil(1) + 1)) + Math.ceil(1) == 1 ? 'Уборка' : Math.floor(Math.random() * (Math.floor(4) - Math.ceil(1) + 1)) + Math.ceil(1) == 2 ? 'Ремонт' : 'Доставка';
                let icon = type == 'Уборка' ? 'yborka' : type == 'Ремонт' ? 'remont' : 'dostavka';
                let description = type == 'Уборка' ? 'Нужно убрать 3-ох комнатную квартиру. При себе иметь все необходимое для уборки. Детали при встрече' : type == 'Ремонт' ? 'Поремонтировать кран в ванной. Когда включаю воду, с крана начинает капать вода и когда выключаю тоже капает.' : 'Нужно СРОЧНО доставить документы с города Житомир, в Киев. Желающим детили напишу в Чат.';
                console.log("markerJob = " + month + " " + day + " " + time_h + ":" + time_m + " " + type + " " + icon);
                this.creatFakeMarkers(i, myCoords.lat + i*Math.random(), myCoords.lng + i*Math.random(), ''+ month +'.'+ day +'', ''+((i+3)*44)+'', ''+ time_h +':'+ time_m +'', ''+ type +'', ''+ icon +'', description, 0);
            }
        });
    }

    GoogleMapOptions(IdT, locationmarkerLat, locationmarkerLng){
        
        const coordinates = {lat: locationmarkerLat, lng: locationmarkerLng};
        const markerOptions: MarkerOptions = {
            icon: {
                url: './assets/icon/main23.png',
                size: {
                    width: 40,
                    height: 60
                }
            },
            animation: 'DROP',
            position: coordinates,
            id: IdT
        };
        this.markers.push(markerOptions);
    }

    creatFakeMarkers(IdT, locationmarkerLat, locationmarkerLng, DateTask, MoneyTask, TimeTask, TypeTask, IconTask, description, isNew) {
        const latet: any = locationmarkerLat;
        const longet: any = locationmarkerLng;
        console.log('loc = ' + latet + ' ' + longet);

        const coordinates = {lat: locationmarkerLat, lng: locationmarkerLng};

        const markerOptions: MarkerOptions = {
            icon: {
                url: './assets/icon/'+IconTask+'.png',
                size: {
                    width: 40,
                    height: 60
                }
            },
            animation: 'DROP',
            position: coordinates,
            id: IdT,
            date: DateTask,
            money: MoneyTask,
            time: TimeTask,
            typeT: TypeTask,
            descript: description,
            isNew: isNew
        };
        this.DEMO_last_id = IdT;
        console.log("testik2 = " + markerOptions.type);
        this.markers.push(markerOptions);
        
    }

    removeMarkers() {
        for (let i = 0; i < this.markers.length; i++) {
            if (this.markers[i].setMap) {
                this.markers[i].setMap(null);
            }
        }
    }

    myPositionMarker(map, mylocation) {
        const marker = new google.maps.Marker({
            map: map,
            disableAutoPan: true,
            icon: {
                url: './assets/icon/locc.svg'
            },
            anchorPoint: new google.maps.Point(0, -29)
        });
        setInterval(function() {
            marker.setPosition(mylocation);
        }, 1000);

        marker.setVisible(true);

        /*var cityCircle = new google.maps.Circle({
          strokeColor: '#bd690f',
          strokeOpacity: 0.8,
          strokeWeight: 2,
          fillColor: '#e07c10',
          fillOpacity: 0.25,
          map: map,
          center: mylocation,
          radius: 1
        });*/
        const zoom = map.zoom;
    }

    createPopupClass() {
        document.getElementById('content').hidden = false;

        function Popup(position, content) {
            this.position = position;

            content.classList.add('popup-bubble');

            // This zero-height div is positioned at the bottom of the bubble.
            const bubbleAnchor = document.createElement('div');
            bubbleAnchor.classList.add('popup-bubble-anchor');
            bubbleAnchor.appendChild(content);

            // This zero-height div is positioned at the bottom of the tip.
            this.containerDiv = document.createElement('div');
            this.containerDiv.classList.add('popup-container');
            this.containerDiv.appendChild(bubbleAnchor);

            // Optionally stop clicks, etc., from bubbling up to the map.
            google.maps.OverlayView.preventMapHitsAndGesturesFrom(this.containerDiv);
        }

        // ES5 magic to extend google.maps.OverlayView.
        Popup.prototype = Object.create(google.maps.OverlayView.prototype);

        /** Called when the popup is added to the map. */
        Popup.prototype.onAdd = function () {
            this.getPanes().floatPane.appendChild(this.containerDiv);
        };

        /** Called when the popup is removed from the map. */
        Popup.prototype.onRemove = function () {
            if (this.containerDiv.parentElement) {
                this.containerDiv.parentElement.removeChild(this.containerDiv);
            }
        };

        /** Called each frame when the popup needs to draw itself. */
        Popup.prototype.draw = function () {
            const divPosition = this.getProjection().fromLatLngToDivPixel(this.position);

            // Hide the popup when it is far out of view.
            const display =
                Math.abs(divPosition.x) < 4000 && Math.abs(divPosition.y) < 4000 ?
                    'block' :
                    'none';

            if (display === 'block') {
                this.containerDiv.style.left = divPosition.x + 'px';
                this.containerDiv.style.top = divPosition.y + 'px';
            }
            if (this.containerDiv.style.display !== display) {
                this.containerDiv.style.display = display;
            }
        };

        return Popup;
    }

    // loading
    async presentLoading() {
        const loading = await this.loadingController.create({
            spinner: 'crescent',
            duration: 2000,
            message: 'Почекайте будь-ласка...',
            translucent: true,
            cssClass: 'custom-class custom-loading'
        });
        return await loading.present();
    }

    hideMenu() {
        this.tabs.mainMenu(true);
    }

    changeSearchHeaderMode() {

        if (document.getElementById('headForm').classList.contains('opened')) {
            document.getElementById('headForm').classList.add('closed');
            document.getElementById('headForm').classList.remove('opened');
            anime({
                targets: '.workNearYou',
                translateY: -200,
                duration: 1000,
                easing: 'cubicBezier(.5, .05, .1, .3)'
            });
            anime({
                targets: 'ion-buttons',
                translateX: 500,
                duration: 500,
                easing: 'cubicBezier(.5, .05, .1, .3)'
            });
            anime({
                targets: '.searchHeader .header',
                opacity: 0,
                duration: 1000,
                easing: 'cubicBezier(.5, .05, .1, .3)'
            });
            anime({
                targets: '#pac-input',
                width: '65vw',
                translateY: -85,
                translateX: 40,
                duration: 1000,
                easing: 'cubicBezier(.5, .05, .1, .3)'
            });
            anime({
                targets: 'ion-avatar',
                width: 80,
                height: 80,
                paddingTop: 20,
                paddingRight: 20,
                paddingBottom: 20,
                paddingLeft: 22,
                translateY: -25,
                translateX: -140,
                duration: 1000,
                easing: 'cubicBezier(.5, .05, .1, .3)'
            });
            anime({
                targets: '#proposerRang',
                opacity: 0,
                duration: 100,
                easing: 'cubicBezier(.5, .05, .1, .3)'
            });
            anime({
                targets: '.searchHeader',
                height: 100,
                duration: 1000,
                easing: 'cubicBezier(.5, .05, .1, .3)'
            });
            anime({
                targets: '.searchHeader .hider',
                translateY: -140,
                duration: 1000,
                easing: 'cubicBezier(.5, .05, .1, .3)'
            });
            anime({
                targets: '#map_canvas',
                translateY: 80,
                easing: 'cubicBezier(.5, .05, .1, .3)'
            });
        } else if (document.getElementById('headForm').classList.contains('closed')) {
            document.getElementById('headForm').classList.add('opened');
            document.getElementById('headForm').classList.remove('closed');
            anime({
                targets: '.workNearYou',
                translateY: 0,
                duration: 1000,
                easing: 'cubicBezier(.5, .05, .1, .3)'
            });
            anime({
                targets: 'ion-buttons',
                translateX: 0,
                duration: 1000,
                easing: 'cubicBezier(.5, .05, .1, .3)'
            });
            anime({
                targets: '.searchHeader .header',
                opacity: 1,
                duration: 1000,
                easing: 'cubicBezier(.5, .05, .1, .3)'
            });
            anime({
                targets: '#pac-input',
                width: '85vw',
                translateY: 0,
                translateX: 0,
                duration: 1000,
                easing: 'cubicBezier(.5, .05, .1, .3)'
            });
            anime({
                targets: 'ion-avatar',
                width: 100,
                height: 100,
                paddingTop: 25,
                paddingRight: 25,
                paddingBottom: 25,
                paddingLeft: 27,
                translateY: 0,
                translateX: 0,
                duration: 1000,
                easing: 'cubicBezier(.5, .05, .1, .3)'
            });
            anime({
                targets: '#proposerRang',
                opacity: 1,
                duration: 300,
                easing: 'cubicBezier(.5, .05, .1, .3)'
            });
            anime({
                targets: '.searchHeader',
                height: 260,
                duration: 1000,
                easing: 'cubicBezier(.5, .05, .1, .3)'
            });
            anime({
                targets: '.searchHeader .hider',
                translateY: 0,
                duration: 1000,
                easing: 'cubicBezier(.5, .05, .1, .3)'
            });

            anime({
                targets: '#map_canvas',
                translateY: 250,
                easing: 'cubicBezier(.5, .05, .1, .3)'
            });
        }
    }

}


