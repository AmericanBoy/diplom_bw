import { Component, OnInit } from '@angular/core';
import { GoogleChartInterface } from 'ng2-google-charts/google-charts-interfaces';
import anime from 'animejs/lib/anime.es';


@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.page.html',
  styleUrls: ['./my-profile.page.scss'],
})
export class MyProfilePage implements OnInit {

  public pieChart: GoogleChartInterface;

  ionViewDidEnter() {
    this.loadSimplePieChart();
    anime({
      targets: 'ion-card, .backAvatar',
      translateY: -10,
      opacity: 1,
      delay: anime.stagger(100, {start: 500})
    });
  }
  loadSimplePieChart() {
    this.pieChart = {
      chartType: 'PieChart',
      dataTable: [
        ['Task', 'Hours per Day'],
        ['', 3],
        ['', 7],
      ],
     // opt_firstRowIsData: true,
      options: {
        //enableInteractivity: false,
       // height: '100px' ,
        width: '300px' ,
        backgroundColor: 'transparent',
        colors: ['#1D2127', '#EB9817'],
        pieHole: 0.7,
        pieSliceBorderColor: 'none',
        pieSliceText: 'none',
        legend: 'none',
      },
    };
  }
  constructor() {
  }

  ngOnInit() {
  }

}
