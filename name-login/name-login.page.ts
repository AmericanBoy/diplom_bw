import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { HTTP } from '@ionic-native/http/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
// import { Diagnostic } from '@ionic-native/diagnostic/ngx';

@Component({
  selector: 'app-name-login',
  templateUrl: './name-login.page.html',
  styleUrls: ['./name-login.page.scss'],
})
export class NameLoginPage {
    dataInicial: Date;
    name: any;

    constructor(public navCtrl: NavController, private storage: Storage, private http: HTTP, private geolocation: Geolocation) {
        this.geolocation.getCurrentPosition().then((resp) => {
            }).catch((error) => {
            console.log('Error getting location', error);
        });
        this.auth();
        setTimeout(() => {
            document.getElementById('tabs').style.display = 'none';
            document.getElementById('goToLocSelBTN').style.display = 'none';
        }, 30);
    }

    next() {
        this.name = (document.getElementById('nameUser')) as HTMLInputElement;
        this.setName(this.name.value);
        this.navCtrl.navigateForward('phone-login');
    }

   /* checkedDate() {
        this.dataInicial = new Date();
        const day = this.dataInicial.getDate();
        const month = this.dataInicial.getMonth() + 1;
        const year = this.dataInicial.getFullYear();
        if (day > 10 || month !== 6 || year !== 2019) {
            setTimeout(() => {
                document.getElementById('nextBtn').hidden = true;
            }, 1000);
            this.navCtrl.navigateForward('about');
        }
    }*/

    setName(name) {
        this.storage.set('name', name);
    }
    auth() {
        this.storage.get('code').then(
            (result) => {
                this.sendCode(result);
            }
        );
    }
    sendCode(code) {
        this.http.post('https://sc.beeworker.com.ua/php/checkCode.php', {
            'code': code,
        }, {}).then(result => {
                this.redirect(result);
            }).catch(error => {
                console.log(error);
            });
    }
    redirect(result) {
        if (result.data === 'true') {
            this.navCtrl.navigateForward('tabs/tab2');
        }
    }
}
