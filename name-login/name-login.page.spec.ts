import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NameLoginPage } from './name-login.page';

describe('NameLoginPage', () => {
  let component: NameLoginPage;
  let fixture: ComponentFixture<NameLoginPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NameLoginPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NameLoginPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
