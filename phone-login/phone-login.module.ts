import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PhoneLoginPage } from './phone-login.page';
import { ModalPagePage } from '../modal-page/modal-page.page';
import { Sim } from '@ionic-native/sim/ngx';

const routes: Routes = [
  {
    path: '',
    component: PhoneLoginPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PhoneLoginPage, ModalPagePage],
  entryComponents: [ModalPagePage],
  providers: [ Sim ]
})
export class PhoneLoginPageModule {}
