import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { ModalPagePage } from '../modal-page/modal-page.page';
import { Sim } from '@ionic-native/sim/ngx';
import { Storage } from '@ionic/storage';
import { HTTP } from '@ionic-native/http/ngx';

@Component({
    selector: 'app-phone-login',
    templateUrl: './phone-login.page.html',
    styleUrls: ['./phone-login.page.scss'],
})
export class PhoneLoginPage implements OnInit {

    countryName: any = 'Ukraine';
    countryNumber: any = '380';
    countryFlag: any = 'https://restcountries.eu/data/ukr.svg';
    clientPhone: any;

    constructor(public navCtrl: NavController, public modalController: ModalController, private sim: Sim, private storage: Storage, private http: HTTP) {
        this.sim.getSimInfo().then(
            (info) => console.log('Sim info: ', info),
            (err) => console.log('Unable to get sim info: ', err)
        );
    }

    next() {
        // this.navCtrl.navigateForward('code-login');
        // Было бы не плохо запилить проверк на номер телефона
        // Предположим что номер трушный
        this.clientPhone = (document.getElementById('numberPhone')) as HTMLInputElement;
        this.setNumberPhone(this.clientPhone.value);
        this.storage.get('name').then(
            (result) => {
                this.setAllDataInBDandSendCode(this.clientPhone, result);
            }
        );
    }
    emailLogin() {
        this.navCtrl.navigateForward('email-login');
    }

    setNumberPhone(phone) {
        phone = '+' + this.countryNumber + phone;
        this.storage.set('phone', phone);
        this.clientPhone = phone;
    }

    setAllDataInBDandSendCode(phone, name) {
        this.http.post('https://sc.beeworker.com.ua/php/phoneAccept.php', {
            'phone': phone,
            'name': name
        }, {})
            .then(result => {
                const temp = JSON.parse(result.data);
                // console.log(JSON.parse(result.data));
                console.log(temp.id_user);
                this.storage.set('id_user', temp.id_user);
                this.navCtrl.navigateForward('code-login');
            })
            .catch(error => {
                console.log(error);
            });
    }

    async onModal() {
        const modal = await this.modalController.create({
            component: ModalPagePage,
            componentProps: {},
        });
        await modal.present();
        const {data} = await modal.onDidDismiss();
        this.countryName = data.name;
        this.countryNumber = data.number;
        this.countryFlag = data.flag;
    }

    ngOnInit() {
        this.storage.get('phone').then(
            (result) => {
                if (result.length !== 0) {
                    this.clientPhone = result.slice(3);
                }
            }
        );
    }
}
