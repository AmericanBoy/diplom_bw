import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PhoneLoginPage } from './phone-login.page';
import { ModalPagePage } from '../modal-page/modal-page.page';
import {HttpClientModule} from '@angular/common/http'; // import
const routes: Routes = [
  {
    path: '',
    component: PhoneLoginPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HttpClientModule,

    RouterModule.forChild(routes)
  ],
  // providers: [HTTP],
  declarations: [PhoneLoginPage, ModalPagePage],
  entryComponents: [ModalPagePage]
})
export class PhoneLoginPageModule {}
