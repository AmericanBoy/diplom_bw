import {NgModule, OnInit} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IonicStorageModule } from '@ionic/storage';

import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { RouterModule, Routes } from '@angular/router'; // map
import { HTTP } from '@ionic-native/http/ngx';

import {mainMap} from 'src/app/MapGenerationClass/mainMap';

import { HammerGestureConfig, HAMMER_GESTURE_CONFIG} from '@angular/platform-browser';
import * as Hammer from 'hammerjs';

export class CustomHammerConfig extends HammerGestureConfig{
    overrides = {
        'pan': {
            direction: Hammer.DIRECTION_ALL
        }
    };
}

import {HttpClientModule} from '@angular/common/http'; // import
import {
    GoogleMaps,
    GoogleMap,
    GoogleMapsEvent,
    Marker,
    MarkerOptions,
    GoogleMapsAnimation,
    MyLocation,
    Environment
} from '@ionic-native/google-maps';



// @ts-ignore
// @ts-ignore
// @ts-ignore
@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  // tslint:disable-next-line:max-line-length
  imports: [BrowserModule, IonicModule.forRoot(), IonicStorageModule.forRoot({name: 'beeworker'}), AppRoutingModule, HttpClientModule],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
    NativeGeocoder,
    HTTP,
    HttpClientModule,
    Geolocation,
    mainMap,
    { provide: HAMMER_GESTURE_CONFIG, useClass: CustomHammerConfig}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
