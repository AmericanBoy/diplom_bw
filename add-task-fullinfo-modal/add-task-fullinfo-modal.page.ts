import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { NavParams } from '@ionic/angular';
import {Storage} from '@ionic/storage';
import {HTTP} from '@ionic-native/http/ngx';
import anime from 'animejs/lib/anime.es';
import {mainMap} from 'src/app/MapGenerationClass/mainMap';

@Component({
  selector: 'app-add-task-fullinfo-modal',
  templateUrl: './add-task-fullinfo-modal.page.html',
  styleUrls: ['./add-task-fullinfo-modal.page.scss'],
})
export class AddTaskFullinfoModalPage implements OnInit {
  time: any;

  dateStartTask: any;
  dateEndTask: any;
  moneyTask: any;
  timeStartTask: any;
  timeEndTask: any;
  typeTask: any;
  late: any;
  long: any;
  descriptionTask: any;
  id_USER: any; 
  datetimeTask_start: any;
  datetimeTask_end: any;
  testt: Date = new Date('YYYY-MM-DD HH:mm:ss');
  testt2: any;

  DEMO_MyTasks = [];
  constructor( private mainmap: mainMap, public modal: ModalController, public navCtrl: NavController, public storage: Storage, navParams: NavParams, public http: HTTP) {
      const fullPrice = (+navParams.get('chMoney') + (navParams.get('chMoney') * 0.05));
    setTimeout(() => {
      document.getElementById('chAboutt').innerText = navParams.get('aboutTask');
      document.getElementById('chAddresss').innerText = navParams.get('addressTask');
      document.getElementById('chTime').innerText = navParams.get('choosedStartTime') + ' - ';
      document.getElementById('chDate').innerText = navParams.get('choosedStartDate') + ' ';
      document.getElementById('chEndDatee').innerText = navParams.get('choosedEndDate') + ' ';
      document.getElementById('chEndTimee').innerText = navParams.get('choosedEndTime') + ' ';
      document.getElementById('chMoneyy').innerText = String(fullPrice);

      this.storage.get('location').then(
        (result) => {
          this.late = result.lat;
          this.long = result.lng;
          this.mainmap.loadLeafMap(parseFloat(this.late), parseFloat(this.long), "map_canvas_add_task_modal");
          //alert("lat = " + this.late);
        }
      );

      this.storage.get('id_user').then(
        (result) => {
          this.id_USER = result;
        }
      );
      this.dateStartTask = navParams.get('choosedStartDate');
      this.dateEndTask = navParams.get('choosedEndDate');
      this.moneyTask = String(fullPrice);
      this.timeStartTask = navParams.get('choosedStartTime');
      this.timeEndTask = navParams.get('choosedEndTime');
      this.typeTask = "Уборка";
      this.descriptionTask = navParams.get('aboutTask');
      this.datetimeTask_start = this.dateStartTask + " " + this.timeStartTask;
      this.datetimeTask_end = this.dateEndTask + " " + this.timeEndTask;
      this.testt2 = this.dateEndTask+ " " + this.timeEndTask;
      this.testt = new Date(this.testt2);
      console.log("testt = " + this.testt);
    }, 200);
    
  }

  sendMarkerDataOnServer() {
    // console.log("SEND = " + this.dateTask + " " + this.moneyTask + " " + this.timeTask + " " + this.typeTask + " " + this.late + " " + this.long + " " + this.descriptionTask);
    // url - ссылка на файл обрадотчик. Первые скобки {параметры} -
    // пока что только координаты для теста, опзже можно будет передавать и другие данные

    //type: this.typeTask - сказать максу что бы добавил поле для типа работы
    /*this.http.post('https://sc.beeworker.com.ua/task/write/', {date_start: this.datetimeTask_start, date_end: this.datetimeTask_end, money: this.moneyTask, lat: this.late, lng: this.long, description: this.descriptionTask, id_customer: this.id_USER, id_categories_task: 1}, {})
    .then(data => {
      console.log(this.timeEndTask);
      this.navCtrl.navigateForward('tabs/tab2');
      this.dismiss();
    });*/
    //alert("this.late = " + this.late);
    let coords = {lat: this.late, lng: this.long};
    //alert("coords = " + coords.lat);
    this.storage.set('newAddTask', 1);
    this.storage.set('newDataTask', this.dateStartTask);
    this.storage.set('newMoneyTask', this.moneyTask);
    this.storage.set('newTypeTask', this.typeTask);
    this.storage.set('newTimeTask', this.timeStartTask);
    this.storage.set('newDescriptionTask', this.descriptionTask);
    this.storage.set('newCoordsTask', coords);

    this.DEMO_MyTasks.push({type_full_task: this.typeTask, time_start_full_task: this.timeStartTask, time_end_full_task: "16:15", date_start_full_task: this.dateStartTask, money_full_task: this.moneyTask, zakazchik: "Я", date_end_full_task: "04.10.2019", id_full_task_info: 1});
    
    this.storage.get('MyTasks').then(result => {
      if (result != null) {
        let temp = JSON.parse(result);
        console.log("1 = " + temp[0]);
        console.log("2 = " + temp[0]['type_full_task']);
        this.DEMO_MyTasks.push(temp[0]);
        this.storage.set('MyTasks', JSON.stringify(this.DEMO_MyTasks));
        console.log('My Tasks = ', JSON.parse(result));
      }
      else {
        this.storage.set('MyTasks', JSON.stringify(this.DEMO_MyTasks));
      }
    });
    this.navCtrl.navigateForward('tabs/tab2');
    this.dismiss();
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modal.dismiss({
      'dismissed': true
    });
  }

  ngOnInit(): void {
  }
}
