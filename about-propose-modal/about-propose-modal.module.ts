import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { AboutProposeModalPage } from './about-propose-modal.page';



const routes: Routes = [
  {
    path: '',
    component: AboutProposeModalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AboutProposeModalPage]
})
export class AboutProposeModalPageModule {}
