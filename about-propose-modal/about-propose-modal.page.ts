import { Component, OnInit } from '@angular/core';
import { NavParams } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import anime from 'animejs/lib/anime.es';
import {HTTP} from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-about-propose-modal',
  templateUrl: './about-propose-modal.page.html',
  styleUrls: ['./about-propose-modal.page.scss'],
})

export class AboutProposeModalPage {
  id_user: any;
  id_task: any;
  money: any;

  constructor(public storage: Storage, public http: HTTP, public modal: ModalController, public navParams: NavParams) {
    setTimeout(() => {
      this.money = navParams.get('taskMoney');
      this.storage.get('id_user').then((result) => { this.id_user = result });
      this.storage.get('id_task').then((result) => { this.id_task = result });
    }, 300);
  }

  changeToFixPrice() {
    const toogleValue = document.getElementById('fixPriceChanger') as HTMLInputElement;
    const walSign = document.getElementById('ion-input-0-lbl');
    if (!toogleValue.checked) {
      anime({
        targets: '.hourRate',
        opacity: 0,
        height: '0px',
        easing: 'easeOutQuad',
        duration: 500
      });
      anime({
        targets: '#ion-input-0-lbl',
        opacity: 0,
        easing: 'easeOutQuad',
        direction: 'alternate',
        duration: 500
      });
      setTimeout(() => {
        walSign.innerText = 'грн.';
      }, 300);
    } else {
      anime({
        targets: '.hourRate',
        opacity: 1,
        height: '200px',
        easing: 'easeInQuad',
        duration: 500
      });
      anime({
        targets: '#ion-input-0-lbl',
        opacity: 0,
        easing: 'easeOutQuad',
        direction: 'alternate',
        duration: 500
      });
      setTimeout(() => {
        walSign.innerText = 'грн./год.';
      }, 300);

    }
  }
  addPrice() {
    const priceVal = document.getElementById('customPrice') as HTMLInputElement;
    priceVal.value = String(Number(priceVal.value) + 10);
  }

  sendProposalToServer(){
    let price = document.getElementById('customPrice') as HTMLInputElement;
    let areaProposal = document.getElementById('areaProposal')as HTMLInputElement;
    let checkBox = document.getElementById('fixPriceChanger') as HTMLInputElement;
    let muchTime = document.getElementById('muchTime') as HTMLInputElement;
    let minTimeJob = muchTime.value['lower'];
    let maxTimeJob = muchTime.value['upper'];
    let fixOrtimeJob = checkBox.checked == false ? 0 : 1;//Если 0 - оплата по часам, есил 1 - фиксированая оплата
    this.getMarkerDataOnServer(this.id_task, this.id_user, price.value, minTimeJob, maxTimeJob, price.value, areaProposal.value, fixOrtimeJob);
  }

  //Проверить или отправка данных происходит коректно
  getMarkerDataOnServer(id_task, id_user, price_per_hour, minTimeJob, maxTimeJob, full_price, message, fixOrtimeJob) {
    console.log("id_task = " + id_user);
    this.http.post('https://sc.beeworker.com.ua/offers/set/', {id_task: id_task, id_user: id_user, price_per_hour: fixOrtimeJob == 0 ? price_per_hour : 0, price_approx_hour: fixOrtimeJob == 0 ? String(minTimeJob + "-" + maxTimeJob) : 0, full_price: fixOrtimeJob == 1 ? full_price : 0, message: message}, {})
      .then(data => {
      })
      .catch(error => {
        console.log('error = ', error);
    });
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modal.dismiss({
      'dismissed': true
    });
  }
}
