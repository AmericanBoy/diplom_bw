import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutProposeModalPage } from './about-propose-modal.page';

describe('AboutProposeModalPage', () => {
  let component: AboutProposeModalPage;
  let fixture: ComponentFixture<AboutProposeModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutProposeModalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutProposeModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
