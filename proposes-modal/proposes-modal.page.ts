import { Component, OnInit } from '@angular/core';
import { NavParams } from '@ionic/angular';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-proposes-modal',
  templateUrl: './proposes-modal.page.html',
  styleUrls: ['./proposes-modal.page.scss'],
})
export class ProposesModalPage implements OnInit {

  constructor(public modal: ModalController, public navParams: NavParams) { }

  ngOnInit() {
  }
  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modal.dismiss({
      'dismissed': true
    });
  }
}
