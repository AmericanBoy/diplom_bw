import { Component, OnInit } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal-page',
  templateUrl: './modal-page.page.html',
  styleUrls: ['./modal-page.page.scss'],
})
export class ModalPagePage implements OnInit {
  items: any = [{'name' : 'Test1'}, {'name' : 'Test2'}, {'name' : 'Test3'}];
  public country: any = null;
  public temp: any;
  public onElementClick: any;
  constructor(private http: HTTP, public modalC: ModalController) {
    this.getCountry();
    // this.temp = this.items.name[1];
  }
  ngOnInit() {
  }
  async getCountry() {
    this.http.get('https://restcountries.eu/rest/v2/all?fields=name;callingCodes;flag', {}, {})
        .then(country => {
          this.temp = JSON.parse(country.data);
          this.country = this.temp;
        })
        .catch(error => {
          this.country = error;
        });
  }
  async modalDismiss(event) {
    this.onElementClick = event.target;
    if (this.onElementClick.tagName !== 'ION-ITEM') {
      this.onElementClick = this.onElementClick.parentElement;
    }
    if (this.onElementClick.tagName !== 'ION-ITEM') {
      return false;
    }
    this.modalC.dismiss({
      'name': this.onElementClick.firstElementChild.innerText,
      'number': this.onElementClick.lastElementChild.innerText,
      'flag': this.onElementClick.id,
    });
  }
}
