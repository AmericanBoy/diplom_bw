import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AddTaskPage } from './add-task.page';
import {AddTaskFullinfoModalPage} from '../add-task-fullinfo-modal/add-task-fullinfo-modal.page';
import {ModalPagePage} from '../modal-page/modal-page.page';
import { ChangeAddressTaskPage } from '../change-address-task/change-address-task.page';

const routes: Routes = [
  {
    path: '',
    component: AddTaskPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AddTaskPage, AddTaskFullinfoModalPage, ChangeAddressTaskPage],
  entryComponents: [AddTaskFullinfoModalPage, ChangeAddressTaskPage],

})
export class AddTaskPageModule {}


