import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ModalController, NavController} from '@ionic/angular';
import { DatePipe } from '@angular/common';
import {AddTaskFullinfoModalPage} from '../add-task-fullinfo-modal/add-task-fullinfo-modal.page';
import {Storage} from '@ionic/storage';
import {HTTP} from '@ionic-native/http/ngx';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import { AlertController } from '@ionic/angular';
import {mapStyle} from 'src/app/tab2/mapStyle.js';
import {mainMap} from 'src/app/MapGenerationClass/mainMap';
import { ChangeAddressTaskPage } from '../change-address-task/change-address-task.page';
import {AboutProposeModalPage} from '../about-propose-modal/about-propose-modal.page';
import leaflet from 'leaflet';

declare var google: any;

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.page.html',
  styleUrls: ['./add-task.page.scss'],
  providers: [DatePipe]


})
export class AddTaskPage {
  @ViewChild('Map') mapElement: ElementRef;
  @ViewChild('slides') slides;

  map2: any;
  date: Date = new Date();
  todayDate = this.datePipe.transform(this.date, 'yyyy.MM.dd');
  todayHours = (this.date.getHours() < 10 ? '0' : '') + this.date.getHours();
  todayMinutes = (this.date.getMinutes() < 10 ? '0' : '') + this.date.getMinutes();
  todayTime = '' + this.todayHours + ':' + this.todayMinutes;
  todayEndHours = (this.date.getHours() < 10 ? '0' : '') + (Number(this.todayHours) + 1);
  todayEndTime = '' + (Number(this.todayEndHours) > 23 ? '00' : this.todayEndHours) + ':' + this.todayMinutes;
  minDate: any;

  customAlertOptions: any = {
    header: 'Категорії',
    translucent: true
  };
  dateTask: any;
  moneyTask: any;
  timeTask: any;
  typeTask: any;
  latTask: any;
  lngTask: any;
  UserName: any;
  streetdAdress: any;
  modalService: any;
  descriptionTask: any;

  items: []; // стили карты
  map: any; // карта
  mapOptions: any; // настройки карты
  marker: any; // маркер
  apiKey: any = 'AIzaSyBqQbmzhu1f9KvhMSO9DkUDIO8bDaCJ9_I'; /*API Key*/
  newMarkerLocation: any; // хранит в каких координатах спавнить новыйы маркер
  location = {lat: 50.01, lng: 28.02}; // хранит долготу и широту

    // tslint:disable-next-line:max-line-length
  constructor(private mainmap: mainMap, private datePipe: DatePipe, public navCtrl: NavController, public alertController: AlertController, public geolocation: Geolocation,  public modalController: ModalController, public storage: Storage, public http: HTTP) {
    console.log(this.todayDate);
    this.minDate = this.datePipe.transform(this.date, 'yyyy-MM-dd'); // переписавает минимальную дату в другой формат
    this.storage.get('streetNameTask').then(
        (result) => {
          this.streetdAdress = result;
          console.log('resAdress = ' + result);
        }
      );
      setTimeout(() => {
    this.storage.get('location').then(
      (result) => {
          this.latTask = result.lat;
          this.lngTask = result.lng;
          this.loadLeafMap(parseFloat(this.latTask), parseFloat(this.lngTask), "map_canvas_add_task");
          console.log('resAdress = ' + result);
        }
      );
    }, 200);

    this.storage.get('name').then(
      (result) => {
          this.UserName = result;
          console.log('UserName = ' + result);
      }
    );
}
  slideOpts = {
    initialSlide: 0,
    speed: 1000,
    zoom: {
        toggle: false
    }
  };
    async presentAlert() {
        const alert = await this.alertController.create({
            header: 'Низька ціна',
            message: 'Занадто низька ціна для цього завдання. Вкажіть ціну більше 200 грн',
            buttons: ['OK']
        });

        await alert.present();
    }

  async onModal() {
        const chStartTime = document.getElementById('chStartTime') as HTMLInputElement;
        const choosedStartDate = document.getElementById('chStartDate') as HTMLInputElement;
        const choosedEndDate = document.getElementById('chEndDate') as HTMLInputElement;
        const choosedEndTime = document.getElementById('chEndTime') as HTMLInputElement;
        const aboutTask = document.getElementById('chAbout') as HTMLInputElement;
        const addressTask = document.getElementById('chAddress') as HTMLInputElement;
        const taskMoney = document.getElementById('chMoney') as HTMLInputElement;

        const modal = await this.modalController.create({
            component: AddTaskFullinfoModalPage,
            componentProps: {
                'choosedStartTime' : chStartTime.value,
                'choosedStartDate' : this.datePipe.transform(choosedStartDate.value, 'dd.MM.yyyy'),
                'choosedEndTime' : choosedEndTime.value,
                'choosedEndDate' : this.datePipe.transform(choosedEndDate.value, 'dd.MM.yyyy') ,
                'aboutTask':  aboutTask.value,
                'addressTask':  addressTask.value,
                'chMoney':  taskMoney.value

            },
        });

      if (Number(taskMoney.value) <= 100) {
          this.presentAlert()
          this.slides.slideTo(4);
      } else if (aboutTask.value.length < 20) {
          console.log('123');
          this.slides.slideTo(1);
      }  else {
          await modal.present();
      }

        const {data} = await modal.onDidDismiss();

    }

  loadLeafMap(lat, lng, idDivMap){
    this.map2 = leaflet.map(idDivMap, {zoomControl:false, attributionControl: false}).setView([lat, lng], 12);
    leaflet.tileLayer('https://api.mapbox.com/styles/v1/rikind/cjzvakb3v0z081epye72gq5m0/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoicmlraW5kIiwiYSI6ImNqenZhYWJsNjAya2MzY21ycHp6amFocmoifQ.6Fm4Du5SOUv1AUz6NC4F8Q', {
      attributions: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
      zoom: 13,
      setView: true
    }).addTo(this.map2);

    var greenIcon = leaflet.icon({
      iconUrl: '../../assets/icon/point.png',
      iconSize: [60, 70]
    });
    this.map2.on('click', function(e) {    
      document.getElementById('map_canvas_add_task').innerHTML = "<div id='map_canvas_add_task'></div>";
    });

    var marker = leaflet.marker([lat, lng], {icon: greenIcon}).addTo(this.map2);
    //this.map2.dragging.disable();
    this.map2.touchZoom.disable();
    this.map2.doubleClickZoom.disable();
    this.map2.scrollWheelZoom.disable();
    this.map2.boxZoom.disable();
    this.map2.keyboard.disable();
  }

    async onModalChangeLocationTask() {

        const modal = await this.modalController.create({
            component: ChangeAddressTaskPage,
            componentProps: {
              'this': this
            },
          cssClass: 'change-address-task'
        });
        return await modal.present();
      // const {data} = await modal.onDidDismiss();
    }

    async onModal2() {

      const modal = await this.modalController.create({
          component: AboutProposeModalPage,
          componentProps: {
          },
        cssClass: 'about-propose-modal'
      });

      return await modal.present().then(() => {
        this.modalService.setActive();
     });;
     // const {data} = await modal.onDidDismiss();
  }

    getMarkerDataOnServer() {
      console.log('send = ' + this.dateTask + " " + this.moneyTask + " " + this.timeTask + " " + this.typeTask + " " + this.latTask + " " + this.lngTask + " " + this.descriptionTask);
      
      //this.http.post('https://sc.beeworker.com.ua/php/set_full_task_info.php', {date: this.dateTask, money: this.moneyTask, time: this.timeTask, type: this.typeTask, lat: this.latTask, lng: this.lngTask, description: this.descriptionTask ,}, {});
    }

    minEndDateTime() {
       const endDate = document.getElementById('chEndDate') as HTMLInputElement;
       const startDate = document.getElementById('chStartDate') as HTMLInputElement;
       const startTime = document.getElementById('chStartTime') as HTMLInputElement;
       const endTime = document.getElementById('chEndTime') as HTMLInputElement;

       endDate.min = startDate.value;
       endDate.value = startDate.value;

       if (this.datePipe.transform(startDate.value, 'yyyy.MM.dd') !== this.todayDate) {
           startTime.min = '00:00';
       } else if (this.datePipe.transform(startDate.value, 'yyyy.MM.dd') === this.todayDate) {
           startTime.min = this.todayHours + ':' + this.todayMinutes;
           startTime.value = this.todayHours + ':' + this.todayMinutes;
       }
    }
    changeEndMinDateTime() {
        const endDate = document.getElementById('chEndDate') as HTMLInputElement;
        const endTime = document.getElementById('chEndTime') as HTMLInputElement;
        if (this.datePipe.transform(endDate.value, 'yyyy.MM.dd') !== this.todayDate) {
            endTime.min = '00:00';

        } else if (this.datePipe.transform(endDate.value, 'yyyy.MM.dd') === this.todayDate) {
            endTime.min  = this.todayEndTime;
            endTime.value = this.todayEndTime;
        }

    }

  // tslint:disable-next-line:use-life-cycle-interface
  next() { // тут должен быть код отправки данных с формы на сервер
    this.navCtrl.navigateForward('tabs/tab2');
  }

    minEndTime() {
        const startTime = document.getElementById('chStartTime') as HTMLInputElement;
        const endTime = document.getElementById('chEndTime') as HTMLInputElement;
        const endDate = document.getElementById('chEndDate') as HTMLInputElement;
        const time = startTime.value.split(':');

        time[0] = ((Number(time[0]) + 1) > 23 ? '00' : (Number(time[0]) + 1).toString());

        if ((Number(time[0]) + 1) > 23) {
            time[0] = '00';
        }

        time[0] = ((Number(time[0]) < 10 ? '0' : '') + (Number(time[0])) + ':' + time[1]);
        console.log(time[0]);

        endTime.value = time[0];
        endTime.min = time[0];
        if (endTime.value.split(':')[0] === '00') {
            const tomorrow = new Date(endDate.value);

            tomorrow.setDate(tomorrow.getDate() + 1);
            endDate.value = this.datePipe.transform(tomorrow, 'yyyy-MM-dd');
            console.log(tomorrow);
        }
    }
}

