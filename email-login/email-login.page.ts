import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import {Storage} from '@ionic/storage';

@Component({
  selector: 'app-email-login',
  templateUrl: './email-login.page.html',
  styleUrls: ['./email-login.page.scss'],
})
export class EmailLoginPage implements OnInit {

  clientEmail: any;

  constructor( public navCtrl: NavController, private storage: Storage, private http: HTTP) { }

  ngOnInit() {
  }

  phoneLogin() {
    this.navCtrl.navigateForward('phone-login');
  }
  next() {
    // this.navCtrl.navigateForward('code-login');
    // Было бы не плохо запилить проверк на номер телефона
    // Предположим что номер трушный
    this.clientEmail = (document.getElementById('clientEmail')) as HTMLInputElement;
    this.storage.set('email', this.clientEmail.value);
    this.storage.get('name').then(
        (result) => {
          this.setAllDataInBDandSendCode(this.clientEmail.value, result);
        }
    );
    this.storage.get('id_user').then(
        (result) => {
          console.log(result);
        }
    );
  }

  setAllDataInBDandSendCode(email, name) {
    this.http.post('https://beeworker.com.ua/php/sendCode.php', {
      'email': email,
      'name': name
    }, {})
        .then(result => {
          const temp = JSON.parse(result.data);
          console.log(temp.id_user);
          this.storage.set('id_user', temp.id_user);
          this.navCtrl.navigateForward('code-login');
        })
        .catch(error => {
          console.log(error);
        });
  }
}
