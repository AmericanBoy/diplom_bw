import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {mapStyle} from 'src/app/tab2/mapStyle.js';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {
    ToastController,
    Platform
} from '@ionic/angular';
import {
    GoogleMaps,
    GoogleMap,
    GoogleMapsEvent,
    Marker,
    MarkerCluster,
    MarkerOptions,
    GoogleMapsAnimation,
    MyLocation,
    MyLocationOptions,
    LocationService,
    Environment,
    HtmlInfoWindow
} from '@ionic-native/google-maps';
import { Injectable } from '@angular/core'
import leaflet from 'leaflet';

@Component({
    selector: 'app-mainMap'
  })

@Injectable()
export class mainMap {
    @ViewChild('map_canvas_about') mapContainer: ElementRef;
    map2: any;

    items: []; // стили карты
    map: GoogleMap; // карта
    location = {lat: null, lng: null};
    apiKey: any = 'AIzaSyBqQbmzhu1f9KvhMSO9DkUDIO8bDaCJ9_I'; /*API Key*/
    
    HTML_URL: any;
    STYLE_URL: any;
    environment: Environment = null;
    
    constructor(public geolocation: Geolocation,
        public toastCtrl: ToastController){
    }

    loadMap(latTask, lngTask, nameDivId) {
        
      console.log("peredal = " + latTask + " " + lngTask);
      Environment.setEnv({
          'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyBqQbmzhu1f9KvhMSO9DkUDIO8bDaCJ9_I',
          'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyBqQbmzhu1f9KvhMSO9DkUDIO8bDaCJ9_I'
      });

      /* Узнаем позицию пользователя */
      this.geolocation.getCurrentPosition().then((position) => {
          this.location.lat =  position.coords.latitude;
          this.location.lng = position.coords.longitude;
      });

      let coord = {lat: latTask, lng: lngTask};
      /* Настройки карты */
      this.items = mapStyle;
      this.map = GoogleMaps.create(nameDivId, {
          camera: {
              target: coord,
              zoom: 14,
              tilt: 45
          },
          styles: this.items,
          disableDefaultUI: true,
          gestureHandling: 'none',
          gestures: {
              scroll: false,
              tilt: false,
              rotate: false,
              zoom: false
            },
          controls: {
            compass: false,
            myLocationButton: false,
            indoorPicker: false,
            zoom: false // Only for Android
          }
      });

      let marker: Marker = this.map.addMarkerSync({
          icon:{
              url: './assets/icon/point.png',
              size: {
                  width: 60,
                  height: 70
                }
          },
          position: coord,
          animation: GoogleMapsAnimation.BOUNCE,
          disableAutoPan: true
      });
    }

    loadLeafMap(lat, lng, idDivMap){
      //alert(lat + " " + lng + " " + idDivMap);
      this.map2 = leaflet.map(idDivMap, {zoomControl:false, attributionControl: false}).setView([lat, lng], 12);
      leaflet.tileLayer('https://api.mapbox.com/styles/v1/rikind/cjzvakb3v0z081epye72gq5m0/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoicmlraW5kIiwiYSI6ImNqenZhYWJsNjAya2MzY21ycHp6amFocmoifQ.6Fm4Du5SOUv1AUz6NC4F8Q', {
        attributions: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
        zoom: 13,
        setView: true
      }).addTo(this.map2);

      var greenIcon = leaflet.icon({
        iconUrl: '../../assets/icon/point.png',
        iconSize: [60, 70]
      });
      this.map2.on('click', function(e) {        
        //alert("HELLO MOTHERFUCKER!!!");      
      });

      var marker = leaflet.marker([lat, lng], {icon: greenIcon}).addTo(this.map2);
      //this.map2.dragging.disable();
      this.map2.touchZoom.disable();
      this.map2.doubleClickZoom.disable();
      this.map2.scrollWheelZoom.disable();
      this.map2.boxZoom.disable();
      this.map2.keyboard.disable();
    }
}