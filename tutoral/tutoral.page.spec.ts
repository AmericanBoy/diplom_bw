import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TutoralPage } from './tutoral.page';

describe('TutoralPage', () => {
  let component: TutoralPage;
  let fixture: ComponentFixture<TutoralPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TutoralPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TutoralPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
