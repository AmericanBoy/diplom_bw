import {Component, OnInit, ViewChild} from '@angular/core';
import {NavController} from '@ionic/angular';


@Component({
  selector: 'app-tutoral',
  templateUrl: './tutoral.page.html',
  styleUrls: ['./tutoral.page.scss'],
})
export class TutoralPage implements OnInit {
  @ViewChild('slides') slides;
  slideOpts = {
    initialSlide: 0,
    speed: 1000,
    zoom: {
      toggle: false
    }
  };


  constructor( public navCtrl: NavController) {

  }

  ngOnInit() {
    this.slides.lockSwipes(true);

  }

    next() {
      this.navCtrl.navigateForward('/tabs/tab2');

    }

  nextSlide() {
    this.slides.lockSwipes(false);
    this.slides.slideNext();
    this.slides.lockSwipes(true);

  }
}
