import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {NavController, Platform} from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import {HTTP} from '@ionic-native/http/ngx';
import {Tab1Page} from '../tab1/tab1.page';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { NavigationExtras } from '@angular/router';
import { Storage } from '@ionic/storage';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {mapStyle} from 'src/app/tab2/mapStyle.js';
import {Subscription} from 'rxjs';

declare var google: any;

@Component({
  selector: 'app-taken-task',
  templateUrl: './taken-task.page.html',
  styleUrls: ['./taken-task.page.scss'],
  providers: [Tab1Page]
})
export class TakenTaskPage {
  @ViewChild('MapTakenTask') mapElement: ElementRef;



    fulltaskArr1 = [];
  ID_Task: any;
  ID_User: any;

  IdTaskCoin: any;
  TaskCode: any;

  customBackActionSubscription: Subscription;

  number: any;
  email: any;
  id_user: any;

  name_auth: any;
  task_name: any;
  price: any;
  task_start_date: any;
  task_end_date: any;
  task_start_time: any;
  task_end_time: any;
  full_task_inf: any;
  zakazchik: any;
  //Map
  items: []; // стили карты
  map: any; // карта
  mapOptions: any; // настройки карты
  marker: any; // маркер
  apiKey: any = 'AIzaSyBqQbmzhu1f9KvhMSO9DkUDIO8bDaCJ9_I'; /*API Key*/
  newMarkerLocation: any; // хранит в каких координатах спавнить новыйы маркер
  location = {lat: null, lng: null}; // хранит долготу и широту
    public unsubscribeBackEvent: any;



    constructor(public alertController: AlertController, public navCtrl: NavController,
      public geolocation: Geolocation, public http: HTTP, public tab: Tab1Page,
                private storage: Storage, private route: ActivatedRoute, private router: Router, public platform: Platform) {
        this.platform.backButton.subscribe(() => {
            this.navCtrl.navigateForward('tabs/tab1');
        });

    this.test();
  }

  getFullTaskDataOnServer(idTask) {
    console.log("idTask = " + idTask);
    this.http.get('https://sc.beeworker.com.ua/php/get_full_task_info.php', {id: idTask}, {})
              .then(data => {
                const temp1 = JSON.parse(data.data);
                console.log("zaparsil = " + temp1['money_full_task']);

                this.task_name = temp1['type_full_task'];
                this.price = temp1['money_full_task'];
                this.task_start_date = temp1['date_start_full_task'];
                this.task_end_date = temp1['date_end_full_task'];
                this.task_start_time = temp1['time_start_full_task'];
                this.task_end_time = temp1['time_end_full_task'];
                this.full_task_inf = temp1['description'];
                this.location.lat = parseFloat(temp1['lat_full_task']);
                this.location.lng = parseFloat(temp1['lng_full_task']);
    });
  }


  test() {
    this.route.queryParams.subscribe(params => {
      this.ID_Task = params['id'];
      this.task_name = params['task_type'];
      this.price = params['price'];
      this.task_start_date = params['task_start_date'];
      this.task_end_date = params['task_end_date'];
      this.task_start_time = params['task_start_time'];
      this.task_end_time = params['task_end_time'];
      this.full_task_inf = params['full_task_inf'];
      this.location = params['location'];
      this.name_auth = params['zakazchik'];
      console.log(this.task_name + " " + this.price + " " + this.task_start_date + " " + this.task_end_date + " " + this.task_start_time + " " +  this.task_end_time + " " + this.full_task_inf + " " + this.name_auth );
      //this.getFullTaskDataOnServer(this.ID_Task);
    });
  }

  async presentAlertPrompt() {
    const alert = await this.alertController.create({
      message: '<ion-grid class="alertBee" style="top: 30vh;' +
          '  align-items: center;\n' +
          '  background: #232528;\n' +
          '  border-radius: 20px;">\n' +
          '    <ion-row align-items-center>\n' +
          '      <ion-col size="12"></ion-col>\n' +
          '      <ion-col size="12"></ion-col>\n' +
          '      <ion-col size="12">\n' +
          '        <ion-thumbnail style="margin-left: 40%;">\n' +
          '          <img src="../../assets/icon/lockCode.png" style=" width:45px; height: 45px">\n' +
          '        </ion-thumbnail>\n' +
          '      </ion-col>\n' +
          '      <ion-col size="12">\n' +
          '        <ion-text">\n' +
          '          <h2 style="font-family: Montserrat-B, sans-serif;\n' +
          '  text-align: center;\n' +
          '  color: #DDDDDD;\n' +
          '  margin-bottom: 0;">Введіть bee-код</h2>\n' +
          '        </ion-text>\n' +
          '      </ion-col>\n' +
          '      <ion-col size="12">\n' +
          '        <ion-text>\n' +
          '          <h5 style="font-family: Montserrat-L, sans-serif;\n' +
          '  text-align: center;\n' +
          '  color: #DDDDDD;\n' +
          '  font-size: 12px !important;' +
          'margin-top: 0;">bee-код знаходиться у волонтерів</h5>\n' +
          '        </ion-text>\n' +
          '      </ion-col>\n' +
          '    </ion-row>\n' +
          '  </ion-grid> ',
      cssClass: 'prompt_alert',
      inputs: [
        {
          name: 'name1',
          type: 'number',
          placeholder: '* * * * *',
          max: 99999,
          min: 0
        }
      ],
      buttons: [
         {
          text: 'Ok',
          handler: data => {

            this.storage.get('id_user').then(
              (result) => {
               this.id_user = result;
              }

            );

            // console.log(data.name1);

            console.log('Confirm Ok');
          }
        }
      ]
    });

   const tempAlert = alert.querySelector('.alert-wrapper') as HTMLInputElement;
   tempAlert.style.background = '#232528';
    tempAlert.style.padding = '0';
    tempAlert.style.borderRadius = '44px';
    // @ts-ignore
      tempAlert.querySelector('.alert-input-group ').style.paddingTop = '0';
      // @ts-ignore
      tempAlert.querySelector('.alert-input ').style.textAlign = 'center';
      // @ts-ignore
      tempAlert.querySelector('.alert-input ').style.color = 'white';
      // @ts-ignore
      tempAlert.querySelector('.alert-input ').style.fontSize = '16pt';
      // @ts-ignore
      tempAlert.querySelector('.alert-button ').style.fontSize = '16pt';
      // @ts-ignore
      tempAlert.querySelector('.alert-button-inner').style.color = '#DDDDDD';
      // @ts-ignore
      tempAlert.querySelector('.alert-button-group').style.justifyContent = 'center';
      // @ts-ignore
      tempAlert.querySelector('.alert-button-inner').style.marginRight = '';

    await alert.present();

  }


  taketask(){
    this.storage.get('id_user').then(
      (result) => {
        this.ID_User = result;
        this.http.get('https://sc.beeworker.com.ua/php/take_task.php', {id_user: String(this.ID_User), id_task: String(this.ID_Task)}, {});
        this.navCtrl.navigateForward('tabs/tab2');
      }
    );
  }

}
