import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TakenTaskPage } from './taken-task.page';

describe('TakenTaskPage', () => {
  let component: TakenTaskPage;
  let fixture: ComponentFixture<TakenTaskPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TakenTaskPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TakenTaskPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
