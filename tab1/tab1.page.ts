import { Component } from '@angular/core';
import {NavController, Platform} from '@ionic/angular';
import {HTTP} from '@ionic-native/http/ngx';
import {TabsPage} from '../tabs/tabs.page';
import { ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NavigationExtras } from '@angular/router';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']

})


export class Tab1Page {
  @ViewChild('taskSlider') slides;
  temp: any;
  htmlTask: any;

  taskArr = [];
  taskArr2 = [];
  MytaskArr = [];
  taskArr3 = [{name_customer: null, how_much_tasks_done: null, how_much_task_done_categor: null, how_long_work_bw: null, money: null}];
  mainTasks: any[] = [];
  taskIdManager = 'task';
  IdVal: any = 1;
  taskIdValueManager: any;

  // taskInfoServer
  taskName: any;
  tasktakedate: any;
  price: any;
  zakazchik: any;
  iconname: any;
  data_vukon: any;
  time_vukon: any;
  ID_TASK: any = 1;

  ID_USER: any;
  public ID_TASK_NAV: any;

  testTask: any;

  slideOpts = {
    initialSlide: 0,
    spaceBetween: 300,
    speed: 1000,
    effect: 'cube',
    zoom: false
  };

  constructor(public navCtrl: NavController, public http: HTTP, private router: Router, public storage: Storage, public platform: Platform,
              public tabs: TabsPage) {
    this.platform.backButton.subscribe(() => {
        this.navCtrl.navigateForward('tabs/tab2');
        console.log('backButton123');
    });
    this.storage.get('id_user').then(
      (result) => {
        this.ID_USER = result;
        console.log('res = ' + result);
        this.add();
      }
    );
  }

  doRefresh(event) {
    this.getTaskDataOnServer();
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 1000);
  }

  add() {
    //this.getTaskDataOnServer();
    for(let i = 0; i < 3; i++){
      this.taskArr2.push({type_full_task: "Уборка", time_start_full_task: "14:00", time_end_full_task: "16:15", date_start_full_task: "02.10.2019", money_full_task: i+2000, zakazchik: "Генадій В.", date_end_full_task: "04.10.2019", id_full_task_info: i+1})
    }

    this.storage.get('MyTasks').then(result => {
      if (result != null) {
        let temp = JSON.parse(result);
        for(let i = 0; i < temp.length; i++){
          this.MytaskArr.push(temp[i]);
        }
      }
    });
    this.ID_TASK++;
  }

  slideToWorker() {
    this.slides.slideTo(0);

    document.getElementById('workerMenuItem').style.color = 'white';
    document.getElementById('taskgiverMenuItem').style.color = 'grey';
  }

  slideToTaskgiver() {
      this.slides.slideTo(1);
      document.getElementById('workerMenuItem').style.color = 'grey';
      document.getElementById('taskgiverMenuItem').style.color = 'white';
  }

  reRouteOnFullInfoJob(event: any) {
   const test3 = event.currentTarget;
   console.log(test3.getElementsByClassName('taskId')[0].value);
   const iD = test3.getElementsByClassName('taskId')[0].value;
   this.ID_TASK_NAV = test3;
   alert("id = " + iD);
   let task_type;
   let price;
   let task_start_date;
   let task_end_date;
   let task_start_time;
   let task_end_time;
   let full_task_inf;
   let location = {lat: 52.10, lng: 28.10};
   let zakazchik;
   for(let i = 0; i < this.taskArr2.length; i++){
     if(iD == this.taskArr2[i].id_full_task_info)
     {
        zakazchik = this.taskArr2[i].zakazchik;
        task_type = this.taskArr2[i].type_full_task;
        price = this.taskArr2[i].money_full_task;
        task_start_date = this.taskArr2[i].date_start_full_task;
        task_end_date = this.taskArr2[i].date_end_full_task;
        task_start_time = this.taskArr2[i].time_start_full_task;
        task_end_time = this.taskArr2[i].time_end_full_task;
        full_task_inf = task_type == 'Уборка' ? 'Нужно убрать 3-ох комнатную квартиру. При себе иметь все необходимое для уборки. Детали при встрече' : task_type == 'Ремонт' ? 'Поремонтировать кран в ванной. Когда включаю воду, с крана начинает капать вода и когда выключаю тоже капает.' : 'Нужно СРОЧНО доставить документы с города Житомир, в Киев. Желающим детили напишу в Чат.';
      }
   }
   const navigationExtras: NavigationExtras = {
    queryParams: {
        id: iD,
        task_type: task_type,
        price: price,
        task_start_date: task_start_date,
        task_end_date: task_end_date,
        task_start_time: task_start_time,
        task_end_time: task_end_time,
        full_task_inf: full_task_inf,
        location: location,
        zakazchik: zakazchik
    }
  };
   this.router.navigate(['tabs/taken-task'], navigationExtras);
   // this.navCtrl.setRoot(anOtherPage);
  }

  getTaskDataOnServer() {

    console.log("id user = " + this.ID_USER);
    this.http.post('https://sc.beeworker.com.ua/php/get_all_task_performer.php', {id: this.ID_USER}, {})
              .then(data => {
                const temp1 = JSON.parse(data.data);
                  for (const task of this.taskArr2) {
                      this.taskArr2.pop();
                  }
                  for (let i = 0; i < temp1.length; i++) {
                      this.taskArr2[i] = temp1[i];
                  }
              });
}

onClick(event) {
  const target = event.target || event.srcElement || event.currentTarget;
}
    hideMenu() {
        this.tabs.mainMenu(true);
    }
returnDataOnTakeTask() {
  return this.ID_TASK_NAV;
}

}
