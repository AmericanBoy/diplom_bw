import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NavController} from '@ionic/angular';
import {Storage} from '@ionic/storage';
import {HTTP} from '@ionic-native/http/ngx';

@Component({
  selector: 'app-shop-page',
  templateUrl: './shop-page.page.html',
  styleUrls: ['./shop-page.page.scss'],
})
export class ShopPagePage implements OnInit {

  sum: any;
  itemTitle: any;
  itemCost: any;
  coinSet: any;

  lotsArr = [];
  boughtLotsArr = [];
  id_user: any;
  id_user2: any;

  constructor(public navCtrl: NavController, private route: ActivatedRoute, private router: Router,
              public http: HTTP, private storage: Storage) {
        this.test(this);
  }

  ngOnInit() {
  }

    doRefresh(event) {
        setTimeout(() => {
           this.getBalanInfo();
           this.getItems();
           this.getBoughtItems();

            event.target.complete();
        }, 2000);
    }

  test(_this) {
    this.route.queryParams.subscribe(params => {
      this.coinSet = params['coins'];
    });

  _this.storage.get('id_user').then(
    (result) => {
      _this.id_user = result;
    }
  );

  _this.getBalanInfo();
  _this.getItems();
  _this.getBoughtItems();

  }


  getBuyInfo(event: any) {
    const tempEv = event.currentTarget;
    const id_item = tempEv.getElementsByClassName('lotID')[0].value;
    console.log(id_item);
    this.id_user2 = this.id_user;
    this.http.get('https://beeworker.com.ua/php/buyItem.php', {idUser: String(this.id_user), idItem: String(id_item)}, {})
              .then(data => {
                const temp = JSON.parse(data.data);
                if (temp.success === 'false') {
                    alert('На вашому рахунку недостатньо коштів, або ви вже купили цей предмет');
                    console.log(temp);
                  this.sum = temp.money;
                } else {
                  this.sum = temp.money;
                  console.log(temp);
                }
              });
    this.getItems();
    this.getBoughtItems();

  }

  getBalanInfo() {
    this.storage.get('id_user').then(
      (result) => {

        this.id_user2 = result;
     this.http.get('https://beeworker.com.ua/php/userBalance.php', {idUser: String(this.id_user2)}, {})
              .then(data => {
                const temp = JSON.parse(data.data);
                this.sum = temp.money;
              });
      }
    );
  }
  getBoughtItems() {
      this.storage.get('id_user').then(
          (result) => {
              this.id_user2 = result;
              this.http.get('https://beeworker.com.ua/php/getBoughtItems.php', {idUser: String(this.id_user2)}, {})
                  .then(data => {
                      const temp = JSON.parse(data.data);

                      for (const l of this.lotsArr) {
                          this.boughtLotsArr.pop();
                      }
                      for (let i = 0; i < temp.length; i++) {
                          this.boughtLotsArr[i] = temp[i];
                      }
                  });
          });
  }

  getItems() {
      this.storage.get('id_user').then(
          (result) => {
              this.id_user2 = result;
              this.http.get('https://beeworker.com.ua/php/getItems.php', {idUser: String(this.id_user2)}, {})
                  .then(data => {
                      const temp = JSON.parse(data.data);
                      for (const l of this.lotsArr) {
                          this.lotsArr.pop();
                      }
                      for (let i = 0; i < temp.length; i++) {
                         this.lotsArr[i] = temp[i];
                      }
                  });
          });
  }
}
