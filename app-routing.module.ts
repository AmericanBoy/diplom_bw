import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'phone-login', loadChildren: './phone-login/phone-login.module#PhoneLoginPageModule' },
  { path: 'code-login', loadChildren: './code-login/code-login.module#CodeLoginPageModule' },
  { path: 'about', loadChildren: './about/about.module#AboutPageModule' },
  { path: 'email-login', loadChildren: './email-login/email-login.module#EmailLoginPageModule' },
  { path: 'shop-page', loadChildren: './shop-page/shop-page.module#ShopPagePageModule' },
  { path: 'alert-page', loadChildren: './alert-page/alert-page.module#AlertPagePageModule' },
  { path: 'add-task', loadChildren: './add-task/add-task.module#AddTaskPageModule' },
  { path: 'my-profile', loadChildren: './my-profile/my-profile.module#MyProfilePageModule' },
  { path: 'tutoral', loadChildren: './tutoral/tutoral.module#TutoralPageModule' },
  { path: 'proposes', loadChildren: './proposes/proposes.module#ProposesPageModule' },

];
@NgModule({
  imports:  [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
