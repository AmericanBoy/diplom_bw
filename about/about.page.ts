import { Component, OnInit } from '@angular/core';
import {ModalController, NavController} from '@ionic/angular';
import { ActionSheetController } from '@ionic/angular';
import anime from 'animejs/lib/anime.es';
import {AboutProposeModalPage} from '../about-propose-modal/about-propose-modal.page';
import {mainMap} from 'src/app/MapGenerationClass/mainMap';
import {Storage} from '@ionic/storage';
import { ActivatedRoute } from '@angular/router';
import {HTTP} from '@ionic-native/http/ngx';
import { LookAddressTaskPage } from '../look-address-task/look-address-task.page';

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage {
  
  streetdAdress: any;
  latTask: any;
  lngTask: any;
  ID_Task: any;

  name_auth = "Нужно допилить";
  task_name: any;
  price: any;
  task_start_date: any;
  task_end_date: any;
  task_start_time: any;
  task_end_time: any;
  full_task_inf: any;
  location: any;
  howMuch: any;
  people: any;
    // tslint:disable-next-line:max-line-length
  constructor(public http: HTTP, private route: ActivatedRoute, public storage: Storage, private mainmap: mainMap, public navCtrl: NavController, public actionSheetController: ActionSheetController, public modalController: ModalController ) {
    this.storage.get('streetNameTask').then(
      (result) => {
        this.streetdAdress = result;
        console.log('resAdress = ' + result);
      }
    );

    this.storage.get('latAbouTask').then(
        (result) => {
            this.latTask = result;
            console.log('resAdress = ' + result);
        }
    );

    this.storage.get('lngAbouTask').then(
        (result) => {
            this.lngTask = result;
            console.log('resAdress = ' + result);
        }
    );
    
    //DEMO VERSION
    this.name_auth = Math.floor(Math.random() * (Math.floor(4) - Math.ceil(1) + 1)) + Math.ceil(1) == 1 ? 'Микола Г.' : Math.floor(Math.random() * (Math.floor(4) - Math.ceil(1) + 1)) + Math.ceil(1) == 2 ? 'Марина Р.' : Math.floor(Math.random() * (Math.floor(4) - Math.ceil(1) + 1)) + Math.ceil(1) == 3 ? 'Генадій О.' : 'Оксана Д.';
    this.howMuch = Math.floor(Math.random() * (Math.floor(4) - Math.ceil(0) + 0)) + Math.ceil(0);
    this.people = this.howMuch == 1 ? 'людина' : 'людини';
    this.storage.get('typeDemoTask').then((result) => { this.task_name = result; });
    this.storage.get('moneyDemoTask').then((result) => { this.price = result; });
    this.storage.get('descriptDemoTask').then((result) => { this.full_task_inf = result; });
    this.storage.get('dateDemoTask').then((result) => { this.task_start_date = result; });
    this.task_end_date = Math.floor(Math.random() * (Math.floor(4) - Math.ceil(1) + 1)) + Math.ceil(1) == 1 ? '24.10' : Math.floor(Math.random() * (Math.floor(4) - Math.ceil(1) + 1)) + Math.ceil(1) == 2 ? '11.10' : Math.floor(Math.random() * (Math.floor(4) - Math.ceil(1) + 1)) + Math.ceil(1) == 3 ? '29.09' : '01.10';
    this.storage.get('timeDemoTask').then((result) => { this.task_start_time = result; });
    this.task_end_time = Math.floor(Math.random() * (Math.floor(4) - Math.ceil(1) + 1)) + Math.ceil(1) == 1 ? '11:00' : Math.floor(Math.random() * (Math.floor(4) - Math.ceil(1) + 1)) + Math.ceil(1) == 2 ? '15:45' : Math.floor(Math.random() * (Math.floor(4) - Math.ceil(1) + 1)) + Math.ceil(1) == 3 ? '18:15' : '10:00';
    this.storage.get('coordsDemoTask').then((result) => { this.location = result; this.mainmap.loadLeafMap(this.location.lat, this.location.lng, "map_canvas_about");;});
  
    //this.loadInfoAboutTask();

  }

  async onModalLookLocationTask() {
    //alert("Hiii");
    const modal = await this.modalController.create({
        component: LookAddressTaskPage,
        componentProps: {
        },
      cssClass: 'look-address-task'
    });
    return await modal.present();
  // const {data} = await modal.onDidDismiss();
}

  loadInfoAboutTask() {
    this.route.queryParams.subscribe(params => {
      this.ID_Task = params['id'];
      this.getFullTaskDataOnServer(this.ID_Task);
    });
  }

  getFullTaskDataOnServer(idTask) {
    console.log("idTask = " + idTask);
    this.http.post('https://sc.beeworker.com.ua/task/read/', {type: "only", id_task: idTask}, {})
      .then(data => {
        const temp1 = JSON.parse(data.data);
        console.log("zaparsil = " + temp1['money_full_task']);

        //вытащить отдельно дату и отдельно время!!!
        this.name_auth = temp1[0]['customer_name'];
        this.task_name = temp1[0]['categories_name'];
        this.price = temp1[0]['money'];
        this.task_start_date = temp1[0]['date_start'];
        this.task_end_date = temp1[0]['date_end'];
        this.task_start_time = temp1[0]['date_start'];
        this.task_end_time = temp1[0]['date_end'];
        this.full_task_inf = temp1[0]['description'];
        this.location.lat = parseFloat(temp1[0]['lat']);
        this.location.lng = parseFloat(temp1[0]['lng']);
        this.mainmap.loadLeafMap(this.location.lat, this.location.lng, "map_canvas_about");
    });
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      buttons: [{
        text: 'Поскаржитись',
        icon: 'flash-off',
        handler: () => {
          console.log('Delete clicked');
        }
      }, {
        text: 'Поділитися',
        icon: 'share',
        handler: () => {
          console.log('Share clicked');
        }
      },   {
        text: 'Відміна',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }
    async onModal() {
        const taskMoney = document.getElementById('taskMoney') as HTMLInputElement;
        const modal = await this.modalController.create({
            component: AboutProposeModalPage,
            componentProps: {
                'taskMoney': taskMoney.value
            },
          cssClass: 'about-propose-modal'
        });
        return await modal.present();
       // const {data} = await modal.onDidDismiss();
    }

  ionViewWillEnter() {
    anime({
      targets: 'ion-card',
      translateY: -50,
      opacity: 1,
      delay: anime.stagger(100, {start: 500})
    });
  }

  toMainMenu() {
  }

    goToProposes() {
        this.navCtrl.navigateForward('/proposes');
    }
}
