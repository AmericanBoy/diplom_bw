import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CodeLoginPage } from './code-login.page';

describe('CodeLoginPage', () => {
  let component: CodeLoginPage;
  let fixture: ComponentFixture<CodeLoginPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CodeLoginPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CodeLoginPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
