import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-code-login',
  templateUrl: './code-login.page.html',
  styleUrls: ['./code-login.page.scss'],
})
export class CodeLoginPage {
  code: any;
  constructor(public navCtrl: NavController, private http: HTTP, private storage: Storage) { }

  next() {
      // this.navCtrl.navigateForward('tabs/tab2');
       this.code = document.getElementById('code') as HTMLInputElement;
       this.code = this.code.value;
       this.sendCode(this.code);
  }
  sendCode(code) {
      this.http.post('https://sc.beeworker.com.ua/php/checkCode.php', {
          'code': code,
      }, {})
          .then(result => {
              this.checkCode(result);
          })
          .catch(error => {
              console.log(error);
          });
  }
  checkCode(result) {
    console.log(result);
    if (result.data === 'true') {
        this.storage.set('code', this.code);
        this.navCtrl.navigateForward('tabs/tab2');
    } else {
      // вывод ошибки о неправильном коде
    }
  }
}
